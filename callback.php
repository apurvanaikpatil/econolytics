<?php
// Script By linked
session_start();
// if (isset($_SESSION['user_info']) or !isset($_SESSION['login'])) {
//     // if user is logged in
//     header("location: freelance-job-search.php"); // redirect user to index page
//     return false;
// }
include 'linkedin_start.php'; // include linked() function
include 'include/config.inc.php'; // include app data
$code = $_GET['code'];

/* Get User Access Token */
$method_ = 1; // method = 1, because we want POST method
//$url_ = "https://www.linkedin.com/uas/oauth2/accessToken";
$url_ = "https://www.linkedin.com/oauth/v2/accessToken";
$header_ = array(
    "Content-Type: application/x-www-form-urlencoded"
);
$data_ = http_build_query(array(
    "client_id" => $client_id,
    "client_secret" => $client_secret,
    "redirect_uri" => $redirect_uri,
    "grant_type" => "authorization_code",
    "code" => $code,
));
$json_ = 1; // json = 1, because we want JSON response
$get_access_token = linked($method_, $url_, $header_, $data_, $json_);
$access_token = $get_access_token['access_token']; // user access token
/* Get User Info */
$method = 0; // method = 0, because we want GET method
$url = "https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))";

$Emailurl = "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))";
// read about field: https://developer.linkedin.com/docs/fields/basic-profile
$header = array(
    "Authorization: Bearer $access_token"
);
$data = 0; // data = 0, because we do not have data
$json = 1; // json = 1, because we want JSON response
$linkedInProfile_info = linked($method, $url, $header, $data, $json);
$linkedInEmail_info = linked($method, $Emailurl, $header, $data, $json);

$user_info = [];
$user_info["firstName"] = $linkedInProfile_info['firstName']['localized']['en_US'];
$user_info["lastName"] = $linkedInProfile_info['lastName']['localized']['en_US'];
$user_info["emailAddress"] = $linkedInEmail_info['elements'][0]['handle~']['emailAddress'];
$user_info['pictureUrl'] = $linkedInProfile_info['profilePicture']['displayImage~']['elements'][0]['identifiers'][0]['identifier'];

//die(json_encode($user_info));
//print_r($user_info);
$_SESSION['user_info'] = $user_info; // save user info in session

$last_id = (string)NULL;
//print_r($_SESSION['callback']);
if ($_SESSION['user_type'] == 'f_linkedin')
{
    // exit;
    

    $sql_flinkedin = "select * from employee where email_id='" . $_SESSION['user_info']['emailAddress'] . "'";
    $exist_linkdin = mysqli_query($dbh, $sql_flinkedin);
    $row1 = mysqli_fetch_array($exist_linkdin);
    $numl = mysqli_num_rows($exist_linkdin);
    if ($numl == 0)
    {

        $_SESSION['freelance_fname'] = $user_info["firstName"];
        $_SESSION['freelance_lname'] = $user_info["lastName"];
        $_SESSION['freelance_email'] = $user_info["emailAddress"];
        $_SESSION['freelance'] = 'freelancer';
        if ($_SESSION['user_info'] != '')
        {
            $user_info = $_SESSION['user_info'];
            $resume = "";
            $hidden_resume = "";

            if (isset($_SESSION['random_string']))
            {

                $random_string = $_SESSION['random_string'];
                $sql_analyst_signup_details = "select * from analyst_signup_details where session_id='" . $_SESSION['random_string'] . "'";
                $exist_sql_analyst_signup_details = mysqli_query($dbh, $sql_analyst_signup_details);
                $row1_sql_analyst_signup_details = mysqli_fetch_array($exist_sql_analyst_signup_details);
                $resume = $row1_sql_analyst_signup_details['resume'];
                $hidden_resume = $row1_sql_analyst_signup_details['hidden_resume'];

            }

            $linkdin_user = "insert into employee set
                      email_id='" . $user_info['emailAddress'] . "',
                       LinkedIn_consent='1',
                      name='" . $user_info['firstName'] . " " . $user_info['lastName'] . "',
                      user_type='f_linkdin',
                      resume='$resume',
                      hidden_resume='$hidden_resume',
                      face_image='" . $user_info['pictureUrl'] . "', created_at=NOW(),updated_at=NOW()";

            // print_r($linkdin_user);
            $rs = mysqli_query($dbh, $linkdin_user) or die(mysqli_error());

            $last_id = mysqli_insert_id($dbh);
                
                $li_email = $user_info['emailAddress'];
                $f_name = $user_info['firstName'] . " " . $user_info['lastName'];

                $qurr = "INSERT INTO `derived_table` SET
                  `candidate_id`='" . $last_id . "',
                            `name`='" . $f_name . "',
                            `email_id`='" . $li_email . "' ";

            if (isset($_SESSION['random_string']))
            {
                $random_string = $_SESSION['random_string'];

                $all_availability = "SELECT * FROM `availability_analyst_signup` WHERE `session_id` = '$random_string'";

                

                //$last_id = mysqli_insert_id($dbh);
                // $last_id = "1526";
                

                $result = mysqli_query($dbh, $all_availability);
                $row1_all_availability = mysqli_fetch_array($result);

                // print_r($row1_all_availability['session_id']);
                extract($row1_all_availability);

                // echo $session_id;
                $rowcount = mysqli_num_rows($result);

                $sql = "INSERT INTO availability (`employee_id`, `months`, `contract_onsite`, `contract_remote`, `permanent`, `milestone`, `slot_from_1`, `slot_to_1`, `slot_from_2`, `slot_to_2`, `slot_from_3`, `slot_to_3`, `agency_onsite`, `agency_remote`)
                        VALUES ('$last_id','$months','$contract_onsite','$contract_remote','$permanent','$milestone','$slot_from_1','$slot_to_1','$slot_from_2','$slot_to_2','$slot_from_3','$slot_to_3','$agency_onsite','$agency_remote')";

                if (mysqli_query($dbh, $sql))
                {
                    // echo 'done';
                    
                }
                
                
            }

            mysqli_query($dbh, $qurr);

            $_SESSION['offline_analyst_email'] = $user_info["emailAddress"];

            $_SESSION['signup_with_google_linkedin'] = '1';

            if (isset($_SESSION['bid_offline']) && $_SESSION['bid_offline'] > 0)
            {
                header('Location: browse_jobs/get_bid_login.php?bid=' . $_SESSION['bid_offline']);
            }
            else if (isset($_SESSION['random_string']))
            {
                //new USER profile page redirect
                header('Location: profile_page/profile.php?new_user=1');
            }
            else
            {
                //redirect to profile page
                header('Location: freelance-create-account-trial1.php');

            }

        }
    }
    elseif (($user_info['emailAddress'] != '') and $numl == 1)
    {

        if ($row1['user_type'] == 'f_linkdin')
        {
            $_SESSION['freelance_fname'] = $user_info["firstName"];
            $_SESSION['freelance_lname'] = $user_info["lastName"];
            $_SESSION['freelance_email'] = $user_info["emailAddress"];
            $_SESSION['freelance'] = 'freelancer';
            if (isset($_SESSION['bid_offline']) && $_SESSION['bid_offline'] > 0)
            {
                header('Location: browse_jobs/get_bid_login.php?bid=' . $_SESSION['bid_offline']);
            }
            else
            {
                header('location:browse_jobs/browse-all-jobs.php');
            }

        }
        else
        {
            header('location:index.php?already_registered');
        }

    }
}
if ($_SESSION['user_type'] == 'c_linkedin')
{

    $sql_clinkedin = "select * from employer where email_id='" . $_SESSION['user_info']['emailAddress'] . "'";
    $exist_linkdin = mysqli_query($dbh, $sql_clinkedin);
    $row1 = mysqli_fetch_array($exist_linkdin);
    $numl = mysqli_num_rows($exist_linkdin);
    if ($numl == 0)
    {
        $_SESSION['client_name'] = $user_info["firstName"];
        $_SESSION['client_company'] = '';
        $_SESSION['client_email'] = $user_info["emailAddress"];
        $_SESSION['freelance'] = 'client';
        if ($_SESSION['user_info'] != '')
        {
            $user_info = $_SESSION['user_info'];
            $linkdin_user = "insert into  employer set
                          email_id='" . $user_info['emailAddress'] . "',
                          company_name='" . $user_info['firstName'] . " " . $user_info['lastName'] . "',
                          user_type='c_linkdin',
                          face_image='" . $user_info['pictureUrl'] . "', registration_timestamp=NOW()";
            // print_r($linkdin_user);
            $rs = mysqli_query($dbh, $linkdin_user) or die(mysqli_error());

            header('location:project_management/client-profile-edit.php');
        }
    }
    elseif (($user_info['emailAddress'] != '') and $numl == 1)
    {
        if ($row1['user_type'] == 'c_linkedin')
        {
            $_SESSION['client_name'] = $user_info["firstName"];
            $_SESSION['client_company'] = '';
            $_SESSION['client_email'] = $user_info["emailAddress"];
            $_SESSION['freelance'] = 'client';
            header('location:project_management/bid-ongoing.php');
        }
        else
        {
            header('location:index.php?already_registered');
        }

    }
}
exit;

if ($_SESSION['callback'] == "subscribe_settings.php")
{
    header('location:subscribe_settings.php');
}
else
{
    header("location: freelance-job-search.php");
}

header("location: freelance-profile.php"); // redirect user to index page

?>
