<?php
    $page_menu = "attach_contact";
    session_start();
    include_once("../include/config.inc.php");
    include_once 'ecomail/mailer/send_mail.php';
    include("head.php");
    include("header.php");
    if(($_SESSION['client_email']=='') or ($_SESSION['freelance']=='')){
        echo "<script>document.location.href='index.php';</script>";
        exit;
    }
?>

<style type="text/css">
    .total_paid {
        font-size: 24px;
        font-weight: bold;
        color: #27dc27;
    }
    .table-custom th {
        padding: 10px 20px !important;
    }
    .table-custom td {
        padding: 10px 20px !important;
    }

.left-box {
        border: 1px solid #bfbcbc;
    }
 .dashboard-box {
        margin-top: 36px;
    }
.action-btn-box a {
    padding: 18px 30px;
    float: right;
    margin-right: 0px;
}
.textbox-header
{
        border: none;
        color: grey !important;
        background-color: unset;
        border-bottom: 1px solid black;
        border-radius: 0px !important;
        text-shadow: unset;
}
.form-group .form-control{
    padding: 5px;
}
</style>
<body>
<!-- start of code for attach email -->
<?php
if (isset($_POST['attach_email'])) {
    $client_email = $_SESSION['client_email'];

    // print_r($_REQUEST);
    extract($_REQUEST);
    $my_attach_email = $attach_email;
    $my_attach_password = md5($attach_password);
    $my_attach_name = $attach_name;

    //check if email already present in employer
     $sql1="SELECT * from employer where email_id='$attach_email'";
                        $res1=mysqli_query($dbh,$sql1);
                        $rowcount1=mysqli_num_rows($res1);
                        $row1=mysqli_fetch_array($res1);

                         $sql2="SELECT * from employer_add_contacts where attach_email='$attach_email'";
                        $res2=mysqli_query($dbh,$sql2);
                        $rowcount2=mysqli_num_rows($res2);


                        if($rowcount1!=0){
                            // echo "<script> alert('Email id already exists.Contact Econolytics for help');</script>";
                              $output='<div id="lead_form" class="alert alert-danger pull-left">Unable to add the user. As this user is already registered as a client with us. if you still want to add them as a sub-user please write to us at info@econolytics.in</div>';

                        }
                        elseif($rowcount2 !=0){
                            $output='<div id="lead_form" class="alert alert-danger pull-left">Unable to add the user. As this user is already registered as a client with us. if you still want to add them as a sub-user please write to us at info@econolytics.in</div>';

                        }
                         else {
                                     $client_email1=$_SESSION['client_email'];
                                    $sql_client1="SELECT * FROM employer where email_id='$client_email1' ";
                                    $rs_client1=mysqli_query($dbh,$sql_client1);
                                    $row_client1=mysqli_fetch_array($rs_client1);
                                    $client_id1=$row_client1['id'];
                                    $client_name1 = $row_client1['company_name'];

                                    $sql_attach = "INSERT INTO employer_add_contacts (employer_email,employer_id,attach_email,attach_password,name,company_name) values ('$client_email','$client_id1','$my_attach_email','$my_attach_password','$my_attach_name','$client_name1') ";
                                    if(mysqli_query($dbh, $sql_attach)) {
                                            // echo "New record created successfully";

                                                       //send mail password and login
                                       $to = $my_attach_email;
                                 // $to = 'vishal.pawar1312@gmail.com';
                                     // $to = 'maheshadevole@gmail.com';
                                    $to_name = $my_attach_name;
                                    // print_r($to);
                                    $params = array(
                                        'contact_name' => $my_attach_name,
                                        'attach_email' => $my_attach_email,
                                        'attach_password' => $attach_password,
                                        'client_name' => $client_name1,
                                    );
                                // echo $row_client['company_name'];exit();
                            //for analyst..... mail
                            // $to = $row_employee['email_id'];

                            $user_indicator = 3;
                            // $attachment = "";
                            // $attachment = ["","/var/www/html/eco_test/uploads/resume/microsoft.pdf"];
                            $mailStatus = create_n_send(90, $params, $to, $to_name, $dbh, $attachment, $user_indicator);

                            $to = $client_email;
                            $user_indicator = 1;
                            $mailStatus = create_n_send(90, $params, $to, $to_name, $dbh, $attachment, $user_indicator);



                              $output='<div id="lead_form" class="alert alert-success pull-left" style="width: 100%;">Contact added & Mail sent successfully </div>';


                                        } //end of mysql if
                                        else {
                                            $output='<div id="lead_form" class="alert alert-danger pull-left">Sorry there was an error . Please try again later</div>';
                                            // echo "Error: " . $sql . "<br>" . mysqli_error($dbh);
                                        }
                          }//end of row count else



}//end of if - isset attach

?>
<!-- end of code for attach email -->


<!-- Attach email Modal -->
<div class="modal fade" id="attachemailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="">
      <div class="modal-header" style="padding: 20px;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add User</h4>
      </div>
      <div class="modal-body" style="padding: 20px;">
        <form class="form-inline" action="attach-employer-multiple-contacts.php" method="post">
          <div class="form-group">

            <label for="attach_email">Name</label>
            <input type="text" class="form-control" name="attach_name" id="attach_name" placeholder="John">
            <label for="attach_email">Email</label>
            <input type="email" class="form-control" name="attach_email" id="attach_email" placeholder="abc@xyz.com">
            <label for="attach_email">Password</label>
            <input type="password" class="form-control" name="attach_password" id="attach_password" placeholder="*********">
          </div>
          <button type="submit" class="btn btn-primary button-color">Add</button>
        </form>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary button-color" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>
<!-- end of Attach email Modal-->








    <div class="inner-content-box1">
        <a href="#" class="scrollToTop"><img src="images/arrow_up.png"></a>
        <div class="container">
             <?php include("../include/side1.php");?>
            <section class="dashboard-box">
                <div class="row">

                    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 freelanc-project">

                                                 <?php
                            $client_email=$_SESSION['client_email'];
                            $sql_client="SELECT * FROM employer_add_contacts where employer_email='$client_email' ";
                            $rs_client=mysqli_query($dbh,$sql_client);
                            $row_client=mysqli_fetch_array($rs_client);
                            // print_r($row_client);
                        ?>




							<div >
						  <p><?php echo $output; ?></p>
							</div>

                        <div class="table-search-box clearfix">


                             <a href="#" data-toggle="modal" data-target="#attachemailModal" style="float: right;"><i class="fa fa-plus fa-2x pull right" style="color:grey;"></i> &nbsp &nbsp Add User </a>
                             <br>
                             <br>


                             <!-- display already added contacts -->
                            <div class="table-responsive">

                           <table class="table table-custom text-center">
                              <thead>
                                 <tr>
                                    <th>NAME</th>
                                    <th>Email</th>

                                 </tr>
                              </thead>
                              <tbody>

                                   <?php
                            $client_email=$_SESSION['client_email'];
                            $sql_client="SELECT * FROM employer_add_contacts where employer_email='$client_email' ";
                            $rs_client=mysqli_query($dbh,$sql_client);

                                    if (mysqli_num_rows($rs_client) > 0) {
                                        // output data of each row
                                        while($row_payment = mysqli_fetch_assoc($rs_client)) {
                                        extract($row_payment);
                                    ?>

                                    <tr>
                                        <td><?=$name;?></td>
                                        <td><?=$attach_email;?></td>

                                    </tr>

                                    <?php } } else {

                                    } ?>
                              </tbody>
                           </table>
                        </div>
                             <!-- end of display already added contact -->

                        </div>

                        <!--  -->

                </div>
            </section>
        </div>
    </div>


    <?php include("footer.php") ?>
    <?php include("include/signup.php") ?>




   <!--  <script>
        var id  = "<?php //echo $row['id'];?>";
        var name ="<?php //echo $row['name'];?>";
        var active ="<?php //$row['active_deactive'];?>";
    </script> -->
    <script src="js/jquery.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/jquery.dropdown.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.sliderTabs.js"></script>
    <script src="js/plugins.js"></script>
    <script>
        $.material.init();
        $(".dropdown select").dropdown();
    </script>
    <script>
        //Show Details Toggle
        $(document).ready(function() {
            // $("#lead_form").fadeIn(5000);
            $("#lead_form").show().delay(2000).fadeOut();

        $('.js-view-detail').on('click', function(){
            if ($(this).text() == "Show Details")
              {
                 $(this).text("Hide Details");
                 $(this).siblings('p').html('<i class="fa fa-chevron-up" aria-hidden="true" style="color: grey"></i>');
                 $(this).parents('.box-footer').next().slideDown();
              }
              else
              {
                 $(this).text("Show Details");
                 $(this).siblings('p').html('<i class="fa fa-chevron-down" aria-hidden="true" style="color: grey"></i>');
                 $(this).parents('.box-footer').next().slideUp();
              };
        });

        $('body').on('click', '#details2', function(){
            $(this).parents('.detail-box').find('#hidethis').slideDown();
        });

        $('body').on('click', '#reject1', function(){
            $(this).parents('.detail-box').find('#hidethis').slideUp();
        });

        $('body').on('click', '#asign-btn', function(){
            $(this).parents('.detail-box').find('#welcome').slideDown();
        });

        $('body').on('click', '#cancel-btn', function(){
            $(this).parents('.detail-box').find('#welcome').slideUp();
        });

        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
              $('.scrollToTop').fadeIn();
            } else {
              $('.scrollToTop').fadeOut();
            }
        });

        //Click event to scroll to top
        $('.scrollToTop').click(function(){
            $('html, body').animate({scrollTop : 0},800);
                return false;
            });
        });

        //Get Results
        function getresult(value) {
            $.ajax({
                url : "client-job-ajax.php",
                type : "POST",
                beforeSend:function(){$("#overlay").show();},
                data: {value : value},
                success:function(data) {
                    $('#'+value).html(data);
                    $("#overlay").hide();
                },
                error:function(data) {
                    /* Act on the event */
                }
            });
        }

        //Get First Tab Data First
        getresult("bid_ongoing");

        var loaded = false;
        $(document).on('click','.newly_assigned', function() {
            if(loaded) {
                console.log("Newly Assigned Jobs is already Updated");
                return;
            }
            getresult("newly_assigned");
            loaded = true;
        });

        // $(document).on('click','.in_progress', function() {
        //     if(loaded) {
        //         console.log("In Progress Data is already Updated");
        //         return;
        //     }
        //     getresult("in_progress");
        //     loaded = true;
        // });

        // $(document).on('click','.completed', function() {
        //     if(loaded) {
        //         console.log("Completed Data is already Updated");
        //         return;
        //     }
        //     getresult("completed");
        //     loaded = true;
        // });
    </script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
</body>
</html>