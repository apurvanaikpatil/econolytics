<?php
include_once("include/config.inc.php");
include_once("ecomail/mailer/send_mail.php");
ini_set('display_errors',1);
if(isset($_POST['analyst_id']) && $_POST['analyst_id'] != '') {

//Getting Parameters cleaning it and store it in variables
$challenge_id = mysqli_real_escape_string($dbh,$_POST["challenge_id"]);
$i = mysqli_real_escape_string($dbh,$_POST["i"]);
$analyst_id = mysqli_real_escape_string($dbh,$_POST["analyst_id"]);

//Compare challenge id and analyst id whether they're same if yes, get that challenges row data
$query=mysqli_query($dbh,"SELECT * FROM challenges WHERE challenge_id='".$challenge_id."' AND employee_id='".$analyst_id."'");
$challenge=mysqli_fetch_assoc($query);
//print_r($challenge);

// update into challenge table that start button was clicked
$myquery1 = mysqli_query($dbh, "UPDATE challenges SET click_start_status=1,challenge_start_date = now() WHERE challenge_id='$challenge_id' AND employee_id='$analyst_id'");

//$challenge_type = explode(",",$details['challenge_type']); //Not required as of now
$question = explode("~~~",$challenge['question']);
$total_questions = count($question);

$question_type = explode("~~~",$challenge['question_type']);
$option1 = explode("~~~",$challenge['option1']);
$option2 = explode("~~~",$challenge['option2']);
$option3 = explode("~~~",$challenge['option3']);
$option4 = explode("~~~",$challenge['option4']);
$time_limit = explode("~~~",$challenge['time_limit']);

$document = explode("~~~",$challenge['document']);
foreach ($document as $value) {
    for($j=0;$j<=count($document);$j++) {
	   if(strpos($value, '/'.$j) !== false) { $doc_array[$j] = $value; }
    }
}
//print_r($question);

//print_r($_POST);

?>
	<div class="error-text">

	</div>
	<div class="col-md-12 col-sm-12 col-xs-12 timer_div">
		<!-- Setting Time Limit -->
		<input type="hidden" id="set-time" value="<?php echo $time_limit[$i]; ?>"/>
		<div id="countdown" class="timer">
			<div id='tiles' class="color-full"></div>
			<div class="countdown-label">Time Remaining</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-12 col-xs-12">
		<h4>Question <?php echo $i+1; ?> | Question Type : <?php echo $question_type[$i]; ?></h4>
	</div>
	<div class="row">
	    <div class="form-group col-sm-12">
	        <p><?php echo $question[$i]; ?></p>
	    </div>
	</div>
	<?php if($doc_array[$i] == "") { echo ""; } else {?>
	<div class="row">
	    <div class="form-group col-sm-12">
	        <a class='btn-download btn-xs' target='_blank' href="<?php echo $doc_array[$i];?>">Download Task</a>
	    </div>
	</div>
	<?php } ?>
	<div class="row">
	<div class="form-group col-sm-12">
	  <div class="option">
	    <input type="radio" name="option" id="option1" value="<?php echo $option1[$i]; ?>" style="margin-right:5px;" required><?php echo $option1[$i]; ?>
	  </div>
	</div>
	<div class="form-group col-sm-12">
	  <div class="option">
	    <input type="radio" name="option" id="option2" value="<?php echo $option2[$i]; ?>" style="margin-right:5px;" required><?php echo $option2[$i]; ?>
	  </div>
	</div>

	<div class="form-group col-sm-12">
	  <div class="option">
	    <input type="radio" name="option" id="option3" value="<?php echo $option3[$i]; ?>" style="margin-right:5px;" required><?php echo $option3[$i]; ?>
	  </div>
	</div>
	<div class="form-group col-sm-12">
	  <div class="option">
	    <input type="radio" name="option" id="option4" value="<?php echo $option4[$i]; ?>" style="margin-right:5px;" required><?php echo $option4[$i]; ?>
	  </div>
	</div>
	</div>
	<div class="row">
		<input type="hidden" name="timeout" id="timeout" value="">
		<input type="hidden" id="i" class="i" value="<?php echo $i; ?>">
	    <div class="form-group  col-sm-12 text-right">
			<?php if ($total_questions == $i+1): ?>
				<button type="button" id="next" class="btn-start btn-warning next">Done</button>
			<?php else: ?>
			<button type="button" id="next" class="btn-start btn-warning next">Next</button>
			<?php endif; ?>
	    </div>
	</div>

	<script type="text/javascript">
	    var minutes = $( '#set-time' ).val(); //Getting Time Limits of Challenger in Minutes
	    var target_date = new Date().getTime() + ((minutes * 60 ) * 1000); // set the countdown date
	    var time_limit = ((minutes * 60 ) * 1000); //converting into miliseconds
	    var days, hours, minutes, seconds; // variables for time units
	    var countdown = document.getElementById("tiles"); // get tag element

	    //Calling the function
        getCountdown();
        setInterval(function() { getCountdown(); }, 1000);

	    function getCountdown(){
	        // find the amount of "seconds" between now and target
	        var current_date = new Date().getTime();
	        var seconds_left = (target_date - current_date) / 1000;

	        //changing colours depending on time left from green to orange to red
	        if ( seconds_left >= 0 ) {
	          //console.log(time_limit);
	            if ( (seconds_left * 1000 ) < ( time_limit / 2 ) )  {
	             $( '#tiles' ).removeClass('color-full');
	             $( '#tiles' ).addClass('color-half');
	            }
	            if ( (seconds_left * 1000 ) < ( time_limit / 4 ) )  {
	                $( '#tiles' ).removeClass('color-half');
	                $( '#tiles' ).addClass('color-empty');
	            }

	        //Calculating Day Hours Minutes and Seconds to Put it on Timer
	        days = pad( parseInt(seconds_left / 86400) );
	        seconds_left = seconds_left % 86400;

	        hours = pad( parseInt(seconds_left / 3600) );
	        seconds_left = seconds_left % 3600;

	        minutes = pad( parseInt(seconds_left / 60) );
	        seconds = pad( parseInt( seconds_left % 60 ) );

	        // format countdown string + set tag value
	        countdown.innerHTML = "<span>" + hours + ":</span><span>" + minutes + ":</span><span>" + seconds + "</span>";
	        }
	    }

	    //setting actual timer timeout function
	    setTimeout(
	      function() {
	        $(this).closest("#next").next().find("input:button").click();
	      }, time_limit );

	    function pad(n) {
	        return (n < 10 ? '0' : '') + n;
	    }
	</script>


<?php }

if(isset($_POST['challengeId']) && $_POST['challengeId'] != '') {
	//Creating Variables
	$challengeId = mysqli_real_escape_string($dbh,$_POST["challengeId"]);
	$answers = mysqli_real_escape_string($dbh,$_POST["answers"]);
	$answers = explode("~~~",$answers);
	$timeTaken = mysqli_real_escape_string($dbh,$_POST["timeTaken"]);
	$timeTaken = explode("~~~",$timeTaken);

	//Select from Challenges where Challenge id = ?
	$query = mysqli_query($dbh,"SELECT * FROM challenges WHERE challenge_id='$challengeId'");
	$challenge=mysqli_fetch_array($query);
	$correct_option = explode("~~~",$challenge['correct_option']);
	$question = explode("~~~",$challenge['question']);

	//Checking Query
	if($challenge) {

		//Verifying Users Answer whether its correct or not and accordingly updating challenge_status
		$answered_correct = 0;
		$incorrect = 0;
		$not_answered = 0;

		for($i=0;$i<count($correct_option);$i++) {
			if($answers[$i] == '') {
				$correct[$i] = "Not Answered";
				$not_answered++;
			} else if($correct_option[$i] == $answers[$i]) {
				$correct[$i] = "Correct";
				$answered_correct++;
			} else  { $correct[$i] = "Incorrect"; $incorrect++; }
		}

		$attempted = $answered_correct+$incorrect;
		$total_questions = count($question);
		$score = ceil(($answered_correct/$total_questions)*100);
		$correct = implode(",",$correct);


		if(!empty($answers)) {
		$answers = mysqli_real_escape_string($dbh,$_POST['answers']);
		$timeTaken = mysqli_real_escape_string($dbh,$_POST["timeTaken"]);


		// update into challenge table date and time when challenge was completed
        $myquery3 = mysqli_query($dbh, "UPDATE challenges SET challenge_completed_status=1,challenge_completed_date=now() WHERE challenge_id='$challengeId'");

		$query = mysqli_query($dbh, "UPDATE challenges SET challenge_status='Accepted', selected_options='$answers', time_taken='$timeTaken', answered='$correct', challenge_score='$score%' WHERE challenge_id='$challengeId'");
		}

		echo "<table class='table table-bordered table-striped table-responsive'>
					<thead >
						<tr>
							<th style='text-align:left;'>
								<b>Total Number of Questions</b>
							</th>
							<th style='text-align:left;'>
								<b>".$total_questions."</b>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								Attempted Questions
							</td>
							<td>
								".$attempted."
							</td>
						</tr>
						<tr>
							<td>
								Right Answer
							</td>
							<td>
								".$answered_correct."
							</td>
						</tr>
						<tr>
							<td>
								Wrong Answer
							</td>
							<td>
								".$incorrect."
							</td>
						</tr>
						<tr>
							<td>
								No Answer
							</td>
							<td>
								".$not_answered."
							</td>
						</tr>
						<tr>
							<td>
								Your Score
							</td>
							<td>
								".$score."%
							</td>
						</tr>
						<tr>
						<td colspan='2' align='center'><a href='ask-job-search-status.php' class='btn btn-primary'>OK</a></td>
						</tr>
					</tbody>
				</table>
				";

		//Getting Analyst's Details
		$query = mysqli_query($dbh,"SELECT * FROM employee WHERE id='".$challenge['employee_id']."'");
		$analyst=mysqli_fetch_array($query);

		//Getting Clients Details
		$query2 = mysqli_query($dbh,"SELECT * FROM employer WHERE id='".$challenge['employer_id']."'");
		$client=mysqli_fetch_array($query2);

		//Sending Email to Client
		$to = $client['email_id'];
		$to_name = $client['company_name'];

		$params = array(
			"client_company_name" => $to_name,
			"analyst_name" => $analyst['name'],
		);

		//3  is for important mail
		$user_indicator = 3;
		$attachment = "";
		$mailStatus = create_n_send(17,$params,$to,$to_name,$dbh,$attachment,$user_indicator);

		if($mailStatus) {
			print "<p class='success'>Your scores has been sent to Challenger.</p>";
		} else {
			print "<p class='Error'>Problem in Sending your scores.</p>";
		}

	} else {
		echo "<p>Something Wrong Happen!! Please try again</p>";
	    //echo "Error: " . $query . "<br>" . mysqli_error($dbh); //When you get stuck somewhere turn this on
	}
}

if(isset($_POST['challenge_id_view']) && $_POST['challenge_id_view'] != '') {
	//Creating Variables
	$challengeId = mysqli_real_escape_string($dbh,$_POST["challenge_id_view"]);

	//Select from Challenges where Challenge id = ?
	$query = mysqli_query($dbh,"SELECT * FROM challenges WHERE challenge_id='".$challengeId."'");
	$challenge=mysqli_fetch_array($query);

	$answers = explode("~~~",$challenge['selected_options']);
	$timeTaken = explode("~~~",$challenge['time_taken']);
	$question = explode("~~~",$challenge['question']);
	$answered = explode("~~~",$challenge['answered']);
	//print_r($answered);

	//Checking Query
	if($challenge){

		//Verifying Users Answer whether its correct or not and accordingly updating challenge_status
		$answered_correct = 0;
		$incorrect = 0;
		$not_answered = 0;

		for($i=0;$i<count($answered);$i++) {
			if($answered[$i] == 'Not Answered') {
				$not_answered++;
			} else if($answered[$i] == 'Correct') {
				$answered_correct++;
			} else  { $incorrect++; }
		}

		$attempted = $answered_correct+$incorrect;
		$total_questions = count($question);
		$score = ceil(($answered_correct/$total_questions)*100);

		echo "<table class='table table-bordered table-striped table-responsive'>
					<thead>
						<tr>
							<th style='text-align:left;'>
								<b>Total Number of Questions</b>
							</th>
							<th style='text-align:left;'>
								<b>".$total_questions."</b>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								Attempted Questions
							</td>
							<td>
								".$attempted."
							</td>
						</tr>
						<tr>
							<td>
								Right Answer
							</td>
							<td>
								".$answered_correct."
							</td>
						</tr>
						<tr>
							<td>
								Wrong Answer
							</td>
							<td>
								".$incorrect."
							</td>
						</tr>
						<tr>
							<td>
								No Answer
							</td>
							<td>
								".$not_answered."
							</td>
						</tr>
						<tr>
							<td>
								Your Score
							</td>
							<td>
								".$score."%
							</td>
						</tr>
					</tbody>
				</table>";
	} else {
		echo "<p>Something Wrong Happen!! Please try again</p>";
	    //echo "Error: " . $query . "<br>" . mysqli_error($dbh); //When you get stuck somewhere turn this on
	}
}
?>