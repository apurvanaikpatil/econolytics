<?php
$page = "analyst";
session_start();
include "include/config.inc.php";
//include "include/head.php";
// "include/header.php";
include("head.php");


if (($_SESSION['freelance_email'] == '')) {
    header('Location:index.php?signin=1');
    exit;
}

$F_email = $_SESSION['freelance_email'];

$result=mysqli_query($dbh,"SELECT id, email_id FROM employee WHERE email_id = '$F_email'");

while ($row = $result->fetch_assoc())
{
    $employee_id = $row["id"];
}

$agency_details = mysqli_query($dbh, "SELECT id FROM agency_details WHERE analyst_id = '$employee_id' AND status = 1 LIMIT 1");

if(isset($agency_details) && !empty($agency_details))
{
    while ($row = $agency_details->fetch_assoc())
    {
        $agency_id = $row["id"];
    }

    $_SESSION["agency_id"] = $agency_id;
}

$query = "SELECT id, name, email_id, phone, domain FROM employee WHERE agency_id = $agency_id";
$analyst_details = mysqli_query($dbh, $query);

?>

 <!-- chipt new   -->
<link href="chip/css/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="chip/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<!-- chipt new   -->

<link rel="stylesheet" href="css/tokenize2.min.css">
<link rel="stylesheet" type="text/css" href="css/on-off-switch.css">
<link rel="stylesheet" href="https://www.jqueryscript.net/css/jquerysctipttop.css"  type="text/css">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="css/freelance-profile-update.css">
<style type="text/css">
ul.left-list li a.active {
    background: #f5f7f6;
    color: #47cbdc;
}
.year
{
    margin: 12px -12px 0px 7px;
    float:left !important;
    color:#929292;
}
.take-online{
    margin-top: 10px;
    text-align: right;
    padding-bottom: 30px;
}


    #mobile-number {
        height: 30px;
    }


    .button-centre{
    margin-right: 199px!important;
     }

     .btn-addproject-centre{
        margin-right: 237px!important;
     }

 @media only screen and (max-width: 375px) {
.blueheader p
{
           text-align: center;
}
.available {
    font-weight: normal;
    font-size: 13px;
}
.form-group .custom-upload input[type="file"]
{
        top: 0px;
}
.ui-autocomplete {
z-index: 99999999 !important;
}
.ui-autocomplete.ui-front
{
z-index: 1051;
}
.button-centre{
    margin-right: 1px!important;
     }

.btn-addproject-centre{
        margin-right: 30px!important;
     }

.default-icon {
    margin-top: -13px;
    padding-left: 0px;
}
.dropdownjs::after {
    right: 2px;
    top: 5px !important;
}
.btn-info
{
    padding: 8px 12px !important;
    font-size: 12px !important;
}

#view-resume {

    margin-top: -11px;
}
.take-online{
    margin-top: 10px;
    text-align:center;
    padding-bottom: 25px;
}
/*#image-holder-profile
{
    float: right !important;
}*/
#icon-holder
{
    float: left !important;
    padding-left: 23px;
}
.year
{
    margin: 12px 5px 0px 7px;
}
ul.tagit li.tagit-choice
{
        margin: 5px 5px;
    font-size: 12px;
    padding: 4px 21px 4px 10px;
}
/*.edit-button {
    top: -68px;

}*/
/*.remove-button {
    right: -2px;
    top: -38px;
}*/
}
.ui-autocomplete {
z-index: 99999999 !important;
}
.ui-autocomplete.ui-front
{
z-index: 1051;
}

.no-style-link:hover {
    text-decoration: none;
}

.btn-view-analyst {
    background-color: #4caf50;
    color: #ffffff !important;
}

.btn-detach-analyst {
    background-color: #f44336;
    color: #ffffff !important;
}

.txt-black {
    color: #000000 !important;
}
.client-account-box{
	min-height:400px;
}
	.inner-content-box test-complete-box{
		min-height:400px;
		
	}
	.es-dashboard-left{
		height:unset!important;
	}
	.section_2{
		background: #ffff;
	}
	.es-footer {
		    background-color: #ffffff !important;
	}
	.dashboard-box_new {
    margin-top: 68px;
    padding-bottom: 50px;
}
</style>
 <?php include("include-left-pan.php"); 
 include("new_left_sidebar.php");  ?>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WJNX6BL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="section_2">
<div class="inner-content-box1">

    <div class="container">
        <section class="dashboard-box_new">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="box-white new-white-box clearfix">
                        <?php if($_SESSION['agency_id'] != '') { ?>
                        <div class="col-md-12">
                            <div class="col-md-12 txt-black" style="margin-bottom: 10px;font-weight: bold;">
                                <span>Analyst Details 
                                    <?php if(mysqli_num_rows($analyst_details) > 0 && mysqli_num_rows($analyst_details) <= 5) { ?>
                                    <a href="add_analyst.php">
                                        <button class="btn btn-primary btn-add-analyst button-color" style="float: right;">
                                            Add Analyst
                                        </button>
                                    </a>
                                    <?php } ?>
                                </span>
                            </div>
                            <table class='table'>
                                <thead>
                                    <tr class="text-center txt-black">
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Domain Expertise</th>
                                        <th>View</th>
                                        <th>Detach</th>
                                    </tr>
                                </thead>
                                <tbody class="txt-black">
                                    <?php

                                    if(mysqli_num_rows($analyst_details) > 0) {
                                        while($row = mysqli_fetch_array($analyst_details)) { ?>
                                            <tr class="text-center">
                                                <td><?= $row["name"] ?></td>
                                                <td><?= $row["email_id"] ?></td>
                                                <td><?= $row["phone"] ?></td>
                                                <td><?= $row["domain"] ?></td>
                                                <td><a href="analyst_control.php?mode=e&id=<?= $row["id"] ?>"><button class="btn btn-success btn-view-analyst" style="border-radius: 100px;">View</button></a></td>
                                                <td><a href="analyst_control.php?mode=d&id=<?= $row["id"] ?>"><button class="btn btn-danger btn-detach-analyst" style="border-radius: 100px;">Detach</button></a></td>
                                            </tr>
                                    <?php } } else { ?>
                                        <tr class="text-center">
                                            <td colspan="6"><p>Add your first member.</p></td>
                                        </tr>
                                        <tr>
                                            <td colspan="6">
                                                <center>
                                                    <a href="add_analyst.php"><button class="btn btn-primary btn-add-analyst button-color">Add Analyst</button></a>
                                                </center>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="col-sm-12">
                            <center>
                                <div style="font-size: 20px;font-weight: bold;padding-top: 30px;padding-bottom: 10px;color: #FF0000">Sorry Your Agency is not activated.
                                </div>
                            </center>

                            <center><a href='freelance-profile.php' style="background:#ecc41e;" class='btn btn-primary'>OK</a></center>

                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
	
</div>
</div>
</div>

<?php include("footer.php");  ?>
<script>

$('.es-dashboard-left').height($(".section_2").height()+4);
$('.es-main-menu').height($(".es-dashboard-left").height()+40);
</script>
</body>