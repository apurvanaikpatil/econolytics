<?php
$page = "add_agency";
//include "include/config.inc.php";


?>

 <!-- chipt new   -->
<link href="chip/css/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="chip/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<!-- chipt new   -->

<link rel="stylesheet" href="css/tokenize2.min.css">
<link rel="stylesheet" type="text/css" href="css/on-off-switch.css">
<link rel="stylesheet" href="https://www.jqueryscript.net/css/jquerysctipttop.css"  type="text/css">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="css/freelance-profile-update.css">
<style type="text/css">
ul.left-list li a.active {
    background: #f5f7f6;
    color: #47cbdc;
}
.year
{
    margin: 12px -12px 0px 7px;
    float:left !important;
    color:#929292;
}
.take-online{
    margin-top: 10px;
    text-align: right;
    padding-bottom: 30px;
}


    #mobile-number {
        height: 30px;
    }


    .button-centre{
    margin-right: 199px!important;
     }

     .btn-addproject-centre{
        margin-right: 237px!important;
     }

 @media only screen and (max-width: 375px) {
.blueheader p
{
           text-align: center;
}
.available {
    font-weight: normal;
    font-size: 13px;
}
.form-group .custom-upload input[type="file"]
{
        top: 0px;
}
.ui-autocomplete {
z-index: 99999999 !important;
}
.ui-autocomplete.ui-front
{
z-index: 1051;
}
.button-centre{
    margin-right: 1px!important;
     }

.btn-addproject-centre{
        margin-right: 30px!important;
     }

.default-icon {
    margin-top: -13px;
    padding-left: 0px;
}
.dropdownjs::after {
    right: 2px;
    top: 5px !important;
}
.btn-info
{
    padding: 8px 12px !important;
    font-size: 12px !important;
}

#view-resume {

    margin-top: -11px;
}
.take-online{
    margin-top: 10px;
    text-align:center;
    padding-bottom: 25px;
}
/*#image-holder-profile
{
    float: right !important;
}*/
#icon-holder
{
    float: left !important;
    padding-left: 23px;
}
.year
{
    margin: 12px 5px 0px 7px;
}
ul.tagit li.tagit-choice
{
        margin: 5px 5px;
    font-size: 12px;
    padding: 4px 21px 4px 10px;
}
/*.edit-button {
    top: -68px;

}*/
/*.remove-button {
    right: -2px;
    top: -38px;
}*/
}
.ui-autocomplete {
z-index: 99999999 !important;
}
.ui-autocomplete.ui-front
{
z-index: 1051;
}

.no-style-link:hover {
    text-decoration: none;
}

.btn-view-analyst {
    background-color: #4caf50;
    color: #ffffff !important;
}

.btn-detach-analyst {
    background-color: #f44336;
    color: #ffffff !important;
}

.txt-black {
    color: #000000 !important;
}
.client-account-box{
	min-height:400px;
}
	.inner-content-box test-complete-box{
		min-height:400px;

	}
	.es-dashboard-left{
		height:unset!important;
	}
	.section_2{
		background: #ffff;
	}

	.dashboard-box_new {
    margin-top: 68px;
    padding-bottom: 50px;
}
</style>
 <?php include("profile_header.php");

if (($_SESSION['freelance_email'] == '')) {
    header('Location:index.php?signin=1');
    exit;
}

$F_email = $_SESSION['freelance_email'];
//get employee details
$result=mysqli_query($dbh,"SELECT id, email_id FROM employee WHERE email_id = '$F_email'");

while ($row = $result->fetch_assoc())
{
    $employee_id = $row["id"];
}

$agency=mysqli_query($dbh,"SELECT id FROM agency_details WHERE analyst_id = '$employee_id' LIMIT 1");



 ?>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WJNX6BL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="section_2">
<?php
//Check whether the user already have an agency
if(mysqli_num_rows($agency) > 0) { ?>

<div class="inner-content-box test-complete-box" style="padding-top: 1px;padding-bottom: 50px;">
    <div class="container">
        <div class="client-account-box">
            <div class="form-box clearfix top">
                <div class="  top col-md-6 col-md-offset-3">
                    <div class="row input-boxes" style="margin-top: 25px;">
                        <div class="col-md-12">
                            <center>
                                <div style="font-size: 20px;padding-top: 30px;padding-bottom: 10px;">Sorry. You Already Have An Agency.
                                </div>
                            </center>
                        </div>
                        <center><a href='freelance-profile.php'  style="background-color: #ffcb00 !important;" class='btn btn-primary'>OK</a></center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php } else { ?>

<div class="inner-content-box1">
    <div class="container">
        <section class="dashboard-box_new">
            <form method="POST" id="add_agency_form" action="agency_control.php?mode=i" autocomplete="off">
                <div class="row">
                    
                    <?php
                    //--apurva-code-start-(24 may 2021)--//
                    if(isset($_SESSION['freelance_is_agency_owner']))
                    {
                        $freelance_is_agency_owner = $_SESSION['freelance_is_agency_owner'];
                    }
                    else
                    {
                        $freelance_is_agency_owner =  "no";
                    }
                    //--apurva-code-end-(24 may 2021)--//
                    ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="box-white new-white-box clearfix">
                            <div class="col-md-12">
                                <div class="col-md-12" style="margin-bottom: 10px;">
                                    <span>Agency Details</span>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-static new-form-group">
                                        <label for="agency_name" class="control-label"> Name</label>
                                        <input type="text" class="form-control" name='agency_name' id="agency_name" placeholder="Enter Agency Name" required>
                                        <span class="error-msg"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-static new-form-group">
                                        <label for="agency_email" class="control-label"> Email</label>
                                        <input type="email" class="form-control" name='agency_email' id="agency_email" placeholder="Enter Agency Email" required>
                                        <span class="error-msg"></span>
                                    </div>
                                </div>
                               <div class="col-sm-6">
                                    <div class="form-group label-static new-form-group">
                                        <label for="agency_contact_no" class="control-label"> Contact No.</label>
                                        <input type="text" class="form-control" name='agency_contact_no' id="agency_contact_no" placeholder="Enter Contact No." onKeyDown="if(this.value.length==10 && event.keyCode!=8) return false;"  onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));" required>
                                        <span class="error-msg"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-static new-form-group">
                                        <label for="agency_alt_contact_no" class="control-label"> Alt. Contact No.</label>
                                        <input type="text" class="form-control" name='agency_alt_contact_no' id="agency_alt_contact_no" placeholder="Enter Alt. Contact No." onKeyDown="if(this.value.length==10 && event.keyCode!=8) return false;"  onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));">
                                        <span class="error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-white new-white-box clearfix">
                            <div class="col-md-12">
                                <div class="col-md-12" style="margin-bottom: 10px;">
                                    <span>Location Details</span>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group label-static new-form-group">
                                        <label for="current_location" class="control-label"> Current Location</label>
                                        <input type="text" class="form-control" name='current_location' id="current_location" placeholder="Enter Current Location" autocomplete="off">
                                        <span class="error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-white new-white-box clearfix">
                            <div class="col-md-12">
                                <div class="col-md-12" style="margin-bottom: 10px;">
                                    <span>Bank Details Details</span>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-static new-form-group">
                                        <label for="bank_name" class="control-label"> Bank Name</label>
                                        <input type="text" class="form-control" name='bank_name' id="bank_name" placeholder="Enter Bank Name" required>
                                        <span class="error-msg"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-static new-form-group">
                                        <label for="account_number" class="control-label"> Account Number</label>
                                        <input type="text" class="form-control" name='account_number' id="account_number" placeholder="Enter Account Number" required>
                                        <span class="error-msg"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-static new-form-group">
                                        <label for="ifsc_no" class="control-label"> IFSC/BIC/IBAN</label>
                                        <input type="text" class="form-control" name='ifsc_no' id="ifsc_no" placeholder="Enter IFSC/BIC/IBAN No." required>
                                        <span class="error-msg"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-static new-form-group">
                                        <label for="branch" class="control-label"> Branch</label>
                                        <input type="text" class="form-control" name='branch' id="branch" placeholder="Enter Branch" required>
                                        <span class="error-msg"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group label-static new-form-group">
                                        <label for="account_holder_name" class="control-label"> Account Holder</label>
                                        <input type="text" class="form-control" name='account_holder_name' id="account_holder_name" placeholder="Enter Account Holder" required>
                                        <span class="error-msg"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group new-form-group">
                                        <input type="checkbox" name="acceptance_terms_condition" id="acceptance_terms_condition"> <span class="acceptance_terms_condition">By confirming, you agree to Econolytics <a class="acceptance_terms_condition" href
                                            ="terms-n-conditions.php" target="_blank">Terms of Service</a> ,<a class="acceptance_terms_condition" href="privacy-policy.php" target="_blank">Privacy Policy and Content Policies.</a></span><br>
                                        <span class="error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center" style="margin-top: 25px;">
                            <div class="save-changes">
                                <input type="submit" name="save_agency" value="Save Agency" id="save-agency-action" class="btn btn-primary button-color save_agency_details text-center" style="border-radius: unset;">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>

<?php } ?>

</div>
</div>
<script>
$("#acceptance_terms_condition").change(function(){

  var checkBox = document.getElementById("acceptance_terms_condition");

 if (checkBox.checked == true){
    console.log("abc");
							 $(".acceptance_terms_condition").css("color","unset");
							 $("a.acceptance_terms_condition").css("color"," #ecc41e");



								}
								else{
                                     console.log("xyz");
						 $(".acceptance_terms_condition").css("color","#FF0000");

									return false;

								}

});
</script>
<script>


$('#add_agency_form').submit(function () {

    // Get the Login Name value and trim it
  var checkBox = document.getElementById("acceptance_terms_condition");
															   if (checkBox.checked == false){
							 $(".acceptance_terms_condition").css("color","#FF0000");

								return false;							   }


    // Check if empty of not

});
</script>
<script>
    $(document).ready(function() {
        $("#agency_name").keypress(function(event){
            var inputValue = event.charCode;
            if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)){
                event.preventDefault();
            }
        });

        $("#account_holder_name").keypress(function(event){
            var inputValue = event.charCode;
            if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)){
                event.preventDefault();
            }
        });

    });
</script>
<?php include("profile_footer.php");  ?>

</body>