<?php
session_start();
include_once "include/config.inc.php";
include_once 'ecomail/mailer/send_mail.php';
include_once 'ecomail/mailer/send_mail_repeat.php';

//print($_REQUEST);
//exit();
// $_REQUEST['applied'];
if (isset($_REQUEST['applied'])) {
    $note = $_REQUEST['note'];
    $currency = $_REQUEST['currency'];
    $bid_offered = $_REQUEST['bid_offered'];
    $employee_id = $_REQUEST['employee_id'];
    $job_id = $_REQUEST['job_id'];
    $employer_id_fk = $_REQUEST['employer_id_fk'];
    $current_ctc = $_REQUEST['c_ctc'];
    $expected_ctc = $_REQUEST['e_ctc'];

    //Find Employer Email ID
    $sql = "select email_id,company_name,name,phone from employer where id='$employer_id_fk'";
    $rs = mysqli_query($dbh, $sql);
    $client_details = mysqli_fetch_array($rs);
    //print_r($client_details);

    //Find Bid From And Bid To Value
    $sql3 = "select job_title,bid_from,bid_to,job_skills,domain,job_type,engagement from job_register where job_id='$job_id'";
    $rs3 = mysqli_query($dbh, $sql3);
    $row3 = mysqli_fetch_array($rs3);

    $from = $row3['bid_from'];
    $to = $row3['bid_to'];
    $job_type = $row3['job_type'];
    $engagement = $row3['engagement'];

    function curl_request($url) {
        //echo $url;
        $ch = curl_init($url);
        $result = curl_exec($ch);
    }

	//if (($bid_offered >= $from) and ($bid_offered <= $to) and $row3['job_type'] != "Full Time")
	
     if ($row3['job_type'] != "Full Time") {
		
		if($from!='' and $to!='')
		{
			if(($bid_offered > $to))
			{
				header("location:freelance-job-search.php?msg=4&&job_id=$job_id");
				exit;
			}
		}
	
        $sql_check = "SELECT * FROM eco_applied_jobs  WHERE employee_id_fk='$employee_id' AND job_id_fk = '$job_id'";

       $result=mysqli_query($dbh,$sql_check);
       $rowcount=mysqli_num_rows($result);

        if($rowcount == 0){
        $sql = "INSERT INTO `eco_applied_jobs` (employee_id_fk,currency,job_id_fk,bid_offered,note,employer_id,date_applied,time_applied,timestamp_applied,make_bid) VALUES ('$employee_id','$currency','$job_id','$bid_offered','$note','$employer_id_fk',CURDATE(),CURTIME(),NOW(),1)";
        //echo $sql;
        if (mysqli_query($dbh, $sql)) {

            //Preparing Email
            $to = $client_details['email_id'];
            $to_name = $client_details['company_name'];

            if ($job_type == "Project Basis") {
                if ($engagement == 'daily') {
                    $bid_amount = $currency . $bid_offered . "/Day";
                } else if ($engagement == 'monthly') {
                    $bid_amount = $currency . $bid_offered . "/Month";
                } else if ($engagement == 'hourly') {
                    $bid_amount = $currency . $bid_offered . "/Hour";
                }
            } else {
                $bid_amount = $currency . $bid_offered;
            }

            $params = array(
                'client_company_name' => $to_name,
                'analyst_name' => $_SESSION['freelance_fname'] . " " . $_SESSION['freelance_lname'],
                'job_id' => $job_id,
                'job_title' => $row3['job_title'],
                'job_link' => BASE_PATH . 'freelance-job-search.php',
                'client_name' => $client_details['company_name'],
                'bid_amount' => $bid_amount,
                'technical_skills' => $row3['job_skills'],
                'primary_domain' => $row3['domain'],
                'bid_ongoing_link' => BASE_PATH . 'index.php?callback=project_management/bid-ongoing.php?project='.$job_id.'#'.$job_id,
                'download_analyst_profile_link'=> BASE_PATH .'view-analyst-profile-link-for-dowload.php?analyst_id='.$employee_id,
            );
            //for clients
            $user_indicator = 1;
            $attachment = "";
            // $mailStatus = create_n_send(3, $params, $to, $to_name, $dbh, $attachment, $user_indicator);

            //same mail to econolytics
            $to = 'info@econolytics.in';
            $to_name = 'Econolytics';
            $user_indicator = 3;
            $attachment = "";
            $mailStatus = create_n_send(3, $params, $to, $to_name, $dbh, $attachment, $user_indicator);

            // sleep(10);
            // // Email bid details to analyst himself/herself
            $to1 = $_SESSION['freelance_email'];

            $to_name1 = $_SESSION['freelance_fname'];

            //for clients
            $user_indicator = 0;
            $attachment = "";
            $mailStatus1 = create_n_send(27, $params, $to1, $to_name1, $dbh, $attachment, $user_indicator);

            //Redirect User
            header("Location:freelance-job-search.php?msg=1&&job_id=$job_id");
        } else {
            //echo "Error: " . $sql . "<br>" . mysqli_error($dbh);
		  }
       } else {
	        header("Location:freelance-job-search.php?msg=5&&job_id=$job_id");
		 }
    } elseif ($row3['job_type'] == "Full Time") {

           $sql_check = "SELECT * FROM eco_applied_jobs  WHERE employee_id_fk='$employee_id' AND job_id_fk = '$job_id'";

       $result=mysqli_query($dbh,$sql_check);
       $rowcount=mysqli_num_rows($result);

        if($rowcount == 0){


        $sql = "INSERT INTO `eco_applied_jobs` (employee_id_fk,currency,job_id_fk,bid_offered,note,employer_id,c_ctc,e_ctc,date_applied,time_applied,timestamp_applied,make_bid) VALUES ('$employee_id','$currency','$job_id','$bid_offered','$note','$employer_id_fk','$current_ctc','$expected_ctc',CURDATE(),CURTIME(),NOW(),1)";
        //echo $sql;
        if (mysqli_query($dbh, $sql)) {

            // $to = $client_details['email_id'];
            // $to_name = $client_details['company_name'];


            $params = array(
                'client_company_name' => $to_name,
                'analyst_name' => $_SESSION['freelance_fname'] . " " . $_SESSION['freelance_lname'],
                'job_id' => $job_id,
                'job_title' => $row3['job_title'],
                'client_name' => $client_details['company_name'],
                'technical_skills' => $row3['job_skills'],
                'primary_domain' => $row3['domain'],
                'bid_ongoing_link' => BASE_PATH . 'index.php?callback=project_management/bid-ongoing.php?project='.$job_id.'#'.$job_id,
                'download_analyst_profile_link'=> BASE_PATH .'view-analyst-profile-link-for-dowload.php?analyst_id='.$employee_id,
            );
            //for clients
            $user_indicator = 1;
            $attachment = "";
            // $mailStatus = create_n_send(88, $params, $to, $to_name, $dbh, $attachment, $user_indicator);

            //for analyst
            $to23 = $_SESSION['freelance_email'];
            $to_name23 = $_SESSION['freelance_fname'] . " " . $_SESSION['freelance_lname'];
            $user_indicator = 0;
            $attachment = "";
            $mailStatus = create_n_send(98, $params, $to23, $to_name23, $dbh, $attachment, $user_indicator);


			$to = 'info@econolytics.in';
            $to_name = 'Econolytics';
            $user_indicator = 3;
            $attachment = "";
            $mailStatus = create_n_send(88, $params, $to, $to_name, $dbh, $attachment, $user_indicator);

			header("location:freelance-job-search.php?msg=1&&job_id=$job_id");

        } else {
			}
        }// end of row count
        header("location:freelance-job-search.php?msg=1&&job_id=$job_id");
    } else {
        header("location:freelance-job-search.php?msg=4&&job_id=$job_id");
    }

}

if (isset($_REQUEST['cancel'])) {
    $reason = $_REQUEST['reason'];
    $job_id = $_REQUEST['job_id'];

    //Find Employer Email ID
    $sql = "select em.email_id,em.company_name from employer em, eco_applied_jobs e where e.employer_id = em.id and e.id='$job_id'";
    $rs = mysqli_query($dbh, $sql);
    $client_details = mysqli_fetch_array($rs);
    //print_r($client_details);

    //Find JOb Details
    $sql3 = "select j.job_title from job_register j, eco_applied_jobs e where e.job_id_fk=j.job_id and e.id='$job_id'";
    $rs3 = mysqli_query($dbh, $sql3);
    $row3 = mysqli_fetch_array($rs3);

    $sql = "update eco_applied_jobs set cancel_interview='$reason',cancel_sending_date=NOW(),cancel_status='1' where id='$job_id'";
    if (mysqli_query($dbh, $sql)) {
        //Preparing Email
        $to = $client_details['email_id'];
        $to_name = $client_details['company_name'];
        $params = array(
            'client_company_name' => $to_name,
            'analyst_name' => $_SESSION['freelance_fname'] . " " . $_SESSION['freelance_lname'],
            'job_title' => $row3['job_title'],
        );
        //for clients
        $user_indicator = 1;
        $attachment = '';
        echo $mailStatus = create_n_send(22, $params, $to, $to_name, $dbh, $attachment, $user_indicator);
        header("location:freelance-job-project.php?msg=2");
    }
}


function send_sms_client($number,$new_message)
{
	$apiKey = urlencode('DueIp/mEsBE-gis6RPtNLEu62Rnxw4dUfL8AsJiZax');

	// Message details
	$numbers = array($number);
	$sender = urlencode('TXTLCL');
	$message_text=$new_message;
	$message = rawurlencode($message_text);

	$numbers = implode(',', $numbers);

	// Prepare data for POST request
	$data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message,"unicode"=>
	true);

	// Send the POST request with cURL
	$ch = curl_init('https://api.textlocal.in/send/');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_close($ch);

	// Process your response here
	//echo $response;
}

function short_url($url)
{

	$apiKey = urlencode('DueIp/mEsBE-gis6RPtNLEu62Rnxw4dUfL8AsJiZax');

	// Prepare data for POST request
	$data = 'apikey=' . $apiKey . '&url=' . $url;

	// Send the GET request with cURL
	$ch = curl_init('https://api.textlocal.in/create_shorturl/?' . $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_close($ch);

	// Process your response here

	$data = json_decode($response, TRUE);

	return $data['shorturl'];
}



?>