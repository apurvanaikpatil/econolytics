<?php

// session_start();
// include "include/config.inc.php";
//include "include/head.php";
// "include/header.php";
include("head.php");


?>

 <!-- chipt new   -->
<link href="chip/css/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="chip/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<!-- chipt new   -->

<link rel="stylesheet" href="css/tokenize2.min.css">
<link rel="stylesheet" type="text/css" href="css/on-off-switch.css">
<link rel="stylesheet" href="https://www.jqueryscript.net/css/jquerysctipttop.css"  type="text/css">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="css/freelance-profile-update.css">
<style type="text/css">
ul.left-list li a.active {
    background: #f5f7f6;
    color: #47cbdc;
}
.year
{
    margin: 12px -12px 0px 7px;
    float:left !important;
    color:#929292;
}
.take-online{
    margin-top: 10px;
    text-align: right;
    padding-bottom: 30px;
}


    #mobile-number {
        height: 30px;
    }


    .button-centre{
    margin-right: 199px!important;
     }

     .btn-addproject-centre{
        margin-right: 237px!important;
     }
	 ul.tagit li.tagit-choice .tagit-label:not(a) {
    color: rgb(11, 11, 11);
}

 @media only screen and (max-width: 375px) {
.blueheader p
{
           text-align: center;
}
.available {
    font-weight: normal;
    font-size: 13px;
}
.form-group .custom-upload input[type="file"]
{
        top: 0px;
}
.ui-autocomplete {
z-index: 99999999 !important;
}
.ui-autocomplete.ui-front
{
z-index: 1051;
}
.button-centre{
    margin-right: 1px!important;
     }

.btn-addproject-centre{
        margin-right: 30px!important;
     }

.default-icon {
    margin-top: -13px;
    padding-left: 0px;
}
.dropdownjs::after {
    right: 2px;
    top: 5px !important;
}
.btn-info
{
    padding: 8px 12px !important;
    font-size: 12px !important;
}

#view-resume {

    margin-top: -11px;
}
.take-online{
    margin-top: 10px;
    text-align:center;
    padding-bottom: 25px;
}
/*#image-holder-profile
{
    float: right !important;
}*/
#icon-holder
{
    float: left !important;
    padding-left: 23px;
}
.year
{
    margin: 12px 5px 0px 7px;
}
ul.tagit li.tagit-choice
{
    margin: 5px 5px;
    font-size: 12px;
    padding: 4px 21px 4px 10px;
}
}
.ui-autocomplete {
z-index: 99999999 !important;
}
.ui-autocomplete.ui-front
{
z-index: 1051;
}

.error-msg {
    color: #FF0000;
}
.error-msg-email {
    color: #FF0000;
}

.ui-widget-content {
	border-bottom: unset !important;
	border-top: unset !important;
	border-left: unset !important;
	border-right: unset !important;
}

ul.tagit {
	border-bottom: 1px solid #d8d5d0 !important;
	border-radius: unset !important;
	padding: unset !important;
	margin-top: unset !important;
}

.tagit-new {
    width: 100%;
    margin-left: unset !important;
    margin-top: 7px !important;
}

/*ul.tagit {
    height: 80px;
}*/

.es-dashboard-left{
	height: 675px !important;
}
.tag-editor {

    background: transparent;

    list-style-type: none;
    padding: 0 5px 0 0;
    margin: 0;
    overflow: hidden;
    border: 0 !important;
	 border-bottom: 1px solid #d2d2d2 !important;
    cursor: text;
    font: normal 14px sans-serif;
    color: #555;
    background: #fff;
    line-height: 20px;
	margin-top: 9px !important;
}
ul, ol {
    margin-top: 0;
    margin-bottom: 10px;
}
</style>
<body>

 <?php include("profile_header.php");
if (($_SESSION['freelance_email'] == '')) {
    header('Location:index.php?signin=1');
    exit;
}

$F_email = $_SESSION['freelance_email'];

$result=mysqli_query($dbh,"SELECT id, email_id FROM employee WHERE email_id = '$F_email'");

while ($row = $result->fetch_assoc())
{
    $employee_id = $row["id"];
}

$agency_details = mysqli_query($dbh, "SELECT id FROM agency_details WHERE analyst_id = '$employee_id' AND status = 1 LIMIT 1");

if(isset($agency_details) && !empty($agency_details))
{
    while ($row = $agency_details->fetch_assoc())
    {
        $agency_id = $row["id"];
    }

    $_SESSION["agency_id"] = $agency_id;
}

$query = "SELECT id FROM employee WHERE agency_id = $agency_id";
$analyst_details = mysqli_query($dbh, $query);

if(mysqli_num_rows($analyst_details) == 5)
{
    header("Location: analyst.php");
}



 ?>


<div class="inner-content-box1">
    <div class="container">
        <section class="dashboard-box_new">
            <form method="POST" action="analyst_control.php?mode=i" id="add_analyst_form"  >
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="box-white new-white-box clearfix">
                            <?php if($_SESSION['agency_id'] != '') { ?>
                            <div class="col-md-12">
                                <div class="col-md-12" style="margin-bottom: 10px;">
                                    <span>Analyst Details</span>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-static new-form-group">
                                        <label for="analyst_name" class="control-label"> Name</label>
                                        <input type="text" class="form-control" name='analyst_name' id="analyst_name" required>
                                        <span class="error-msg"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-static new-form-group">
                                        <label for="analyst_email" class="control-label"> Email</label>
                                        <input type="email" class="form-control" onchange="$('.error-msg-email').html('');" name='analyst_email' id="analyst_email" placeholder="Optional">
                                        <span class="error-msg-email"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-static new-form-group">
                                        <label for="analyst_phone_number" class="control-label"> Phone Number</label>
                                    
                                        <input type="text" class="form-control" name='analyst_phone_number' id="analyst_phone_number" maxlength="10" onKeyDown="if(this.value.length==10 && event.keyCode!=8) return false;" placeholder="Optional"  onkeypress="return /\d/.test(String.fromCharCode(((event||window.event).which||(event||window.event).which)));">
                                        <span class="error-msg"></span>
                                    </div>
                                </div>

                                

                                <div class="col-sm-6">
                                    <div class="form-group label-static ">
                                        <label for="skills_tag_3" class="control-label">Domain</label>
                                        <div class="tag-box-row">
                                            <input type="text" class="form-control" style="margin-bottom: 0px !important;
    font-weight: normal !important;
    text-align: left !important;
    padding: 0px !important;
    color: #333 !important;" name="analyst_domain_expertise" id="skills_tag_3" required>
                                            <ul id="allowSpacesTags"></ul>

                                        </div>
                                    </div>
                                </div>

                                <!----//--apurva-code-start-(25 may 2021)--//--->
                                <div class="col-md-12" style="margin-left: -10px;">
                                    <div class="col-sm-6">
                                        <div class="form-group label-static new-form-group">

                                            <label for="analyst_password" class="control-label">Password  &nbsp;</label>
                                            
                                            <input type="Password" name="analyst_password" class="form-control" id="analyst_password">
                                                                                        
                                        </div>
                                    </div>

                                    <div class="col-sm-2" style="margin-top: 15px; margin-left: -70px;">
                                        <label for="analyst_password" class="control-label">&nbsp;</label>

                                        <span id="password_icons">
                                            <i class="fa fa-eye-slash" aria-hidden="true" id="icon_password_close" style="display: inline-block;"></i>

                                            <i class="fa fa-eye" aria-hidden="true" id="icon_password_open" style="display: none;"></i>
                                        </span>

                                    </div>
                                </div>
                                <!----//--apurva-code-start-(25 may 2021)--//--->

                            </div>
                            <?php } else { ?>
                                <div class="col-sm-12">
                                    <center>
                                        <div style="font-size: 20px;font-weight: bold;padding-top: 30px;padding-bottom: 10px;color: #FF0000">Sorry Your Agency is not activated.
                                        </div>
                                    </center>

                                    <center><a href='freelance-profile.php' class='btn btn-primary'>OK</a></center>

                                </div>
                            <?php } ?>
                        </div>
                        <div class="text-center" style="margin-top: 25px;">
                            <div class="success-message" style=""></div>
                            <div class="save-changes">
                                <input type="submit" name="save_analyst" value="Save Analyst" id="save-analyst-action" class="btn btn-primary button-color save_analyst_details text-center" style="border-radius: unset;">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>
</div>
<?php include "profile_footer.php"; ?>

<?php
$sql_chip_domain = "select mname from main_domain group by mname ";
$sql_chip_domain_res = mysqli_query($dbh, $sql_chip_domain);
while ($sql_chip_domain_row = mysqli_fetch_array($sql_chip_domain_res)) {
    $domain_array[] = $sql_chip_domain_row['mname'];
}
$domain_array = json_encode($domain_array);
?>

<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/material.min.js"></script>
<script src="js/ripples.min.js"></script>
<script src="js/jquery.dropdown.js"></script>
<script src="js/bootstrap-rating.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="js/jquery.sliderTabs.js"></script>
<script src="js/nouislider.js"></script>
<script src="js/plugins.js"></script>
<script type="text/javascript" src="js/tokenize2.min.js"></script>
<script src="js/typeahead.min.js"></script>
<script type="text/javascript" src="js/tokenize2.min.js"></script>
<script type="text/javascript" src="textext-master/src/js/textext.core.js"></script>
<script type="text/javascript" src="textext-master/src/js/textext.plugin.prompt.js"></script>
<script type="text/javascript" src="textext-master/src/js/textext.plugin.suggestions.js"></script>
<script type="text/javascript" src="textext-master/src/js/textext.plugin.tags.js"></script>
<script type="text/javascript" src="js/on-off-switch.js"></script>
<script type="text/javascript" src="js/on-off-switch-onload.js"></script>
<script type="text/javascript" src="agency_assets/js/agency_validation.js"></script>
<script src="chip/js/tag-it.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

     $(document).ready(function() {
        $("#analyst_name").keypress(function(event){
            var inputValue = event.charCode;
            if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)){
                event.preventDefault();
            }
        });
    });


    $(document).ready(function() {
        var sampleTags = <?php echo $domain_array; ?>;

        //console.log(sampleTags);


        $('#allowSpacesTags').tagit({
            availableTags: sampleTags,
            allowSpaces:true,
            // This will make Tag-it submit a single form value, as a comma-delimited field.
            singleField: true,
            singleFieldNode: $('#analyst_domain_expertise'),
            tagLimit: 3,
            placeholderText: 'Example: Credit Risk, retail'
        });
    });

	$(function() {
        var sampleTags = <?php echo $domain_array; ?>;


        <?php  $domain_str = str_replace(']', ' ', str_replace('[', ' ', str_replace('"', ' ', $row1['domain'])));  ?>

        var domainString = '<?php echo $row1['domain']; ?>';
        var domain_str = new Array();
        domain_str = domainString.split(",") // this will return an array with strings "1", "2", etc.
        console.log(domain_str);
		// Tag juqery


		$('#skills_tag_3').tagEditor({
			initialTags: domain_str ,
			delimiter: ', ', /* space and comma */
			placeholder: '',
			autocomplete: {
            delay: 0,
            position: {
                collision: 'flip'
            },
            source: <?php echo $domain_array; ?>
        }
		});



	});

 $(document).ready(function() {

    $("#icon_password_close").click(function(){
       $("#icon_password_open").show();
       $("#icon_password_close").hide();

       $("#analyst_password").attr("type", "text");
    });

    $("#icon_password_open").click(function(){
        $("#icon_password_open").hide();
        $("#icon_password_close").show();

        $("#analyst_password").attr("type", "Password");
    });

    $("#analyst_name").keyup(function(event){

        let analyst_name = $("#analyst_name").val(); 

        let = name_lenght = analyst_name.length; 
        
        if(name_lenght >5)
        {
            analyst_name = analyst_name.substring(0, 5);
        }        
       
        let datetime = <?php echo date("Ymd"); ?>;

        let Password = analyst_name+'@'+datetime;
        
        $("#analyst_password").val(Password);
    });
});

$("#add_analyst_form").submit(function(e){

    email_id = "<?= $_SESSION['freelance_email'] ?>";
    analyst_email =  $('#analyst_email').val();
 
    if(email_id==analyst_email){
        $('.error-msg-email').html('You can not add  Agency Email ID');
        e.preventDefault();
        return false;
    }
        
});


</script>
</body>