<?php
include_once "include/config.inc.php";
include_once 'ecomail/mailer/send_mail.php';

if(mysqli_query($dbh,"UPDATE agency_details SET status = 1 WHERE id = " . $_GET["token"]))
{
    $query = "SELECT name, analyst_id FROM agency_details WHERE id = " . $_GET["token"];
    $data = mysqli_query($dbh, $query);
    $result = mysqli_fetch_array($data);
    extract($result);

    $analyst_data = mysqli_query($dbh, "SELECT email_id, name FROM employee WHERE id = $analyst_id");
    $analyst_result = mysqli_fetch_array($analyst_data);

    //agency approve email code here
    if(empty($analyst_result["name"]))
    {
        $analyst_name = "User";
    }
    else
    {
        $analyst_name = $analyst_result["name"];
    }
    
    $params = array(
        'analyst_name' => $analyst_name,
        'analyst_id' => $analyst_id
    );

    $user_indicator = 0;
    $attachment = "";
    $mailStatus = create_n_send(105, $params, $analyst_result["email_id"], $analyst_name, $dbh, "", $user_indicator);
}

?>
<?php include 'header.php' ?>
<div class="inner-content-box test-complete-box" style="padding-top: 50px;padding-bottom: 50px;">
    <div class="container">
        <div class="client-account-box">
            <div class="form-box clearfix top">
				<div class="top col-md-12">
                    <div class="row input-boxes" style="padding-top: 50px; padding-bottom: 50px;">
                        <div class="col-md-12">
                            <center>
                                <div style="font-size: 20px;font-weight: bold;padding-top: 30px;padding-bottom: 10px">Agency <?php echo $name; ?> has been activated.
                                </div>
                            </center>
                        </div>
                        <br><br>
                        <!-- <center><a href='freelance-profile.php' class='btn btn-primary'>OK</a></center> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php"; ?>
</body>
</html>