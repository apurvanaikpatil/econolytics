<?php
$page = "analyst";
// session_start();
// include "include/config.inc.php";
//include "include/head.php";
// "include/header.php";
// include("head.php");
include("profile_header.php");

if (($_SESSION['freelance_email'] == '')) {
    header('Location:index.php?signin=1');
    exit;
}
$agency_id ='';
$F_email = $_SESSION['freelance_email'];

$result=mysqli_query($dbh,"SELECT id, email_id FROM employee WHERE email_id = '$F_email'");

while ($row = $result->fetch_assoc())
{
    $employee_id = $row["id"];
}
// print_r($employee_id);
// exit;
$agency_details = mysqli_query($dbh, "SELECT id FROM agency_details WHERE analyst_id = '$employee_id' AND status = 1 LIMIT 1");

if(isset($agency_details) && !empty($agency_details))
{
    while ($row = $agency_details->fetch_assoc())
    {
        $agency_id = $row["id"];
    }

    $_SESSION["agency_id"] = $agency_id;
}

$query = "SELECT id, name, email_id, phone, domain, key_skills, location, experience, resume  FROM employee WHERE agency_id = $agency_id";
$analyst_details = mysqli_query($dbh, $query);

?>

<!-- chipt new   -->
<link href="chip/css/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="chip/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<!-- chipt new   -->


<script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>
<script src="https://adminlte.io/themes/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyxZM936-gZOFGAVQyl7dmNbon56FvJa4&amp;libraries=places"></script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>


<link rel="stylesheet" href="css/tokenize2.min.css">
<link rel="stylesheet" type="text/css" href="css/on-off-switch.css">
<link rel="stylesheet" href="https://www.jqueryscript.net/css/jquerysctipttop.css" type="text/css">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="css/freelance-profile-update.css">

<style type="text/css">
    @media (min-width: 1200px) {
        .container {
            width: 1230px !important;
        }
    }

    ul.left-list li a.active {
        background: #f5f7f6;
        color: #47cbdc;
    }

    .year {
        margin: 12px -12px 0px 7px;
        float: left !important;
        color: #929292;
    }

    .take-online {
        margin-top: 10px;
        text-align: right;
        padding-bottom: 30px;
    }


    #mobile-number {
        height: 30px;
    }


    .button-centre {
        margin-right: 199px !important;
    }

    .btn-addproject-centre {
        margin-right: 237px !important;
    }

    @media only screen and (max-width: 375px) {
        .blueheader p {
            text-align: center;
        }

        .available {
            font-weight: normal;
            font-size: 13px;
        }

        .form-group .custom-upload input[type="file"] {
            top: 0px;
        }

        .ui-autocomplete {
            z-index: 99999999 !important;
        }

        .ui-autocomplete.ui-front {
            z-index: 1051;
        }

        .button-centre {
            margin-right: 1px !important;
        }

        .btn-addproject-centre {
            margin-right: 30px !important;
        }

        .default-icon {
            margin-top: -13px;
            padding-left: 0px;
        }

        .dropdownjs::after {
            right: 2px;
            top: 5px !important;
        }

        .btn-info {
            padding: 8px 12px !important;
            font-size: 12px !important;
        }

        #view-resume {

            margin-top: -11px;
        }

        .take-online {
            margin-top: 10px;
            text-align: center;
            padding-bottom: 25px;
        }

        /*#image-holder-profile
            {
                float: right !important;
            }*/
        #icon-holder {
            float: left !important;
            padding-left: 23px;
        }

        .year {
            margin: 12px 5px 0px 7px;
        }

        ul.tagit li.tagit-choice {
            margin: 5px 5px;
            font-size: 12px;
            padding: 4px 21px 4px 10px;
        }

        /*.edit-button {
                top: -68px;

            }*/
        /*.remove-button {
                right: -2px;
                top: -38px;
            }*/
    }

    .ui-autocomplete {
        z-index: 99999999 !important;
    }

    .ui-autocomplete.ui-front {
        z-index: 1051;
    }

    .no-style-link:hover {
        text-decoration: none;
    }

    .btn-view-analyst {
        background-color: #4caf50;
        color: #ffffff !important;
    }

    .btn-detach-analyst {
        background-color: #f44336;
        color: #ffffff !important;
    }

    .txt-black {
        color: #000000 !important;
    }

    .client-account-box {
        min-height: 400px;
    }

    .inner-content-box test-complete-box {
        min-height: 400px;

    }

    .es-dashboard-left {
        height: unset !important;
    }

    .section_2 {
        background: #ffff !important;
    }

    .dashboard-box_new {
        margin-top: 68px;
        padding-bottom: 50px;
    }

    .after_dot {
        margin-top: 8px;
    }

    .new-white-box {
        padding-right: 30px;
        background: #f6f5f4;
        border-radius: 5px;
    }
    .prl-40{
      padding: 0 40px;
    }
</style>
<style>
  .table{
    border: 1px solid #dddddd;
    width: 100%;
    /* width: 100px; */
    /* display: table; */
    display: block;
    /* width: 100%; */
    overflow-x: scroll;
    white-space: nowrap;
    }
    .table::-webkit-scrollbar-track {
        background: #f1f1f1;
    }
    .table::-webkit-scrollbar-thumb {
        background: #888;
        border-radius: 20px;
    }
    .table::-webkit-scrollbar {
        width: 5px;
        height: 5px;
        cursor: pointer;
    }

    td
    {
        padding: 10px !important;
    }

  </style>

<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WJNX6BL" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


    <div class="inner-content-box1" style="background: #f6f5f400;">
        <div class="d-flex justify-content-center container">
            <section class="dashboard-box_new">
                <div class="row">

                    <style type="text/css">
                        .faq_section {
                            background-color: #8080801c;
                            margin-top: 50px;
                        }

                        .ft-rt {
                            float: right;
                        }

                        .ft-lt {
                            float: left;
                        }

                        .fa-plus {
                            color: #0000008f !important;
                        }

                        .accordion_icon {

                            margin-top: 7px;
                        }

                        .section_lable {
                            width: 85%;
                        }

                        .section_desc {
                            width: 5%;
                        }

                        .accordion {
                            background-color: #80808000 !important;
                            color: #444;
                            cursor: pointer;
                            padding: 10px;
                            width: 100%;
                            border: none;
                            text-align: left;
                            outline: none;
                            font-size: 15px;
                            transition: 0.4s;
                        }

                        .active,
                        .accordion:hover {
                            background-color: #ccc;
                        }

                        .panel {
                            padding: 0 18px;
                            display: none;
                            background-color: #80808000 !important;
                            overflow: hidden;
                            box-shadow: 0px !important;
                            -webkit-box-shadow: 0px !important;
                        }

                        .section1 {
                            width: 63%;
                            display: inline-block;
                            margin-left: -20px;
                        }

                        .section2 {
                            width: 37%;
                            display: inline-block;
                            margin-left: 10px;
                            margin-right: 3px;
                        }

                        .hr_faq {
                            width: 100%;
                            height: 1px;
                            border: 1px solid #00000012;
                        }

                        .panel_acc {
                            box-shadow: 0px !important;
                            -webkit-box-shadow: 0px !important;
                        }
                    </style>

                    <style type="text/css">
                        .btn {
                            background: #428bca;
                            border: #357ebd solid 0px;
                            border-radius: 3px;
                            color: #fff;
                            display: inline-block;
                            font-size: 14px;
                            padding: 8px 15px;
                            text-decoration: none;
                            text-align: center;
                            min-width: 60px;
                            position: relative;
                            transition: color .1s ease;
                        }

                        .btn:hover {
                            background: #357ebd;
                        }

                        .btn.btn-big {
                            font-size: 18px;
                            padding: 15px 20px;
                            min-width: 100px;
                        }

                        .btn-close {
                            color: #aaaaaa;
                            font-size: 20px;
                            text-decoration: none;
                            padding: 10px;
                            position: absolute;
                            right: 7px;
                            top: 0;
                        }

                        .btn-close:hover {
                            color: #919191;
                        }

                        .modale:before {
                            content: "";
                            display: none;
                            background: rgba(0, 0, 0, 0.6);
                            position: fixed;
                            top: 0;
                            left: 0;
                            right: 0;
                            bottom: 0;
                            z-index: 10;
                        }

                        .opened:before {
                            display: block;
                        }

                        .opened .modal-dialog {
                            /* -webkit-transform: translate(0, 0);
                            -ms-transform: translate(0, 0);
                            transform: translate(0, 0);
                            top: 2%; */
                            /* display: none; */
                        }

                        .modal-dialog {
                            display: none;
                            background: #fefefe;
                            border: #333333 solid 0px;
                            border-radius: 10px;
                            margin-left: -290px;
                            text-align: center;
                            position: absolute;
                            left: 50%;
                            top: 2%;
                            z-index: 11;
                            width: 600px;
                            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.3);
                            /* -webkit-transform: translate(0, -500%);
                            -ms-transform: translate(0, -500%);
                            transform: translate(0, -500%);
                            -webkit-transition: -webkit-transform 0.3s ease-out;
                            -moz-transition: -moz-transform 0.3s ease-out;
                            -o-transition: -o-transform 0.3s ease-out;
                            transition: transform 0.3s ease-out; */
                        }

                        .modal-body {
                            padding: 20px;
                        }

                        .modal-body input {
                            width: 100%;
                            padding: 8px;
                            border: 0px;
                            color: #888;
                            outline: 0;
                            font-size: 14px;
                            font-weight: bold
                        }

                        .modal-header,
                        .modal-footer {
                            padding: 10px 20px;
                        }

                        .modal-header {
                            border-bottom: #eeeeee solid 1px;
                        }

                        .modal-header h2 {
                            font-size: 20px;
                        }

                        .analyst_div {
                            margin-top: 25px;
                        }

                        .div_br_btm {
                          border-bottom: 2px solid #80808057;
                          border-radius: 0 0 10px 10px;
                          border-right: 1px solid rgba(128,128,128,0.07);
                          border-left: 1px solid rgba(128,128,128,0.07);
                        }
                    </style>


                    <!-- Modal -->
                    <div class="modale" aria-hidden="true">
                        <div class="modal-dialog">

                            <!--  <div class="modal-header"> 
                      <a href="#" class="btn-close closemodale" aria-hidden="true">&times;</a>
                        </div> -->

                            <?php 
                                $url = "https://www.econolytics.in/eco_test_1/";
                            ?>
                            <style type="text/css">

                                .modal-body input{
                                    font-weight: 100 !important;
                                }
                                
                                .icon_img {
                                    margin-bottom: 10px;
                                    width: 15px;
                                    margin-top: 7px;
                                    margin-left: 7px;
                                }

                                .div_attch_img {
                                    border: 1px solid #d8d5d5;
                                    width: 28px;
                                    padding: 5px;
                                    border-radius: 5px;
                                    cursor: pointer;
                                    margin-top: 15px;
                                }

                                .error-msg {
                                    color: #FF0000;
                                }

                                .err_hide {
                                    display: none;
                                }

                                .chosen-drop {
                                    width: 400px !important;
                                }

                                .chosen-search {
                                    width: 400px !important;
                                }

                                .chosen-choices{
                                    width: 400px !important;
                                    border: 0px !important;
                                    background-image :none !important;
                                }

                                .chosen-with-drop{
                                    border: 0px !important;
                                    background-image :none !important;
                                }

                                .chosen-results {
                                    width: 400px !important;
                                    /*color: #ecc41e !important;*/
                                }

                                .chosen-default {
                                    width: 200px !important;
                                }

                                .chosen-container-single .chosen-single {
                                    border: 0px !important;
                                    background: none !important;
                                    box-shadow: none !important;
                                }

                                .chosen-single {
                                    width: 500px !important;
                                }

                                .chosen-container-single {
                                    border: 0px !important;
                                    background: none !important;
                                    box-shadow: none !important;
                                }

                                .chosen-container {
                                    border: 0px !important;
                                    box-shadow: none !important;
                                }

                                .active-result .highlighted {
                                    background-color: #ecc41e;
                                    color: white;
                                }


                                .chosen-container-multi .chosen-choices {
                                    border: 0px !important;
                                    box-shadow: none !important;
                                    background-image: none !important;
                                }

                                .chosen-container-multi .chosen-with-drop {
                                    border: 0px !important;
                                    box-shadow: none !important;
                                }

                                .chosen-container-active {
                                    border: 0px !important;
                                    box-shadow: none !important;
                                }

                                #analyst_skills_chosen {
                                    border: 0px !important;
                                    box-shadow: none !important;
                                }

                                .chosen-container-multi .chosen-choices li.search-choice {
                                    border: 0px !important;
                                    box-shadow: none !important;
                                }

                                .chosen-container-multi .chosen-choices li.search-field input[type=text] {
                                    color: #8888889c !important;
                                    font-weight: bold !important;
                                }

                                .input_icon
                                {
                                    color: #ecc41e;
                                    margin-top: 10px;
                                    margin-left: 10px;
                                }
                            </style>

                            <form method="POST" action="analyst_control_new.php" id="add_analyst_form"
                                enctype="multipart/form-data">

                                <div class="modal-body" style="padding:0px !important">

                                    <h2 style="float:left;margin-top: 60px;">
                                        <span style="margin-left: 25px;padding: 40px;">Expert Details</span>
                                        <span id="closed_popup"
                                            style=" float: right; font-size: 23px; margin-top: -35px; margin-right: -30px;">
                                            <i class="fa fa-times" aria-hidden="true" style="cursor: pointer;"></i>
                                        </span>
                                        <div
                                            style="margin-left: 60px;height: 2px;width: 480px;background-color: #ecc41e;margin-top: 15px;">
                                        </div>
                                    </h2>


                                    <input type="hidden" value="<?php echo $_SESSION['agency_id'] ?>" name="agency_id"
                                        id="agency_id">

                                    <div class="col-md-12" style="padding-left: 60px;padding-right: 60px;">

                                        <!--name-->
                                        <div class="col-sm-10 analyst_div div_br_btm">
                                            <div class="form-group label-static new-form-group">

                                                <span for="analyst_name" class=""> Name</span>

                                                <div style="">

                                                    <div style="float: left;width: 10%">
                                                        <img class="icon_img"
                                                            src="<?php echo $url;?>uploads/Analyst_img/User.png">
                                                    </div>

                                                    <div style="float: right;width: 90%">
                                                        <input type="text" class="" name='analyst_name'
                                                            id="analyst_name" required placeholder="Enter Name">

                                                        <div class="error-msg err_hide" id="err_analyst_name">
                                                            <br>Please enter you name
                                                        </div>
                                                    </div>

                                                </div>

                                                <span class="error-msg"></span>
                                            </div>
                                        </div>

                                        <!--email-->
                                        <div class="col-sm-10 analyst_div div_br_btm">
                                            <div class="form-group label-static new-form-group">

                                                <span for="analyst_email" class=""> Email</span>

                                                <div style="">

                                                    <div style="float: left;width: 10%">
                                                        <i class="input_icon fa fa-envelope" aria-hidden="true"></i>
                                                    </div>

                                                    <div style="float: right;width: 90%">
                                                        <input type="text" class="" name='analyst_email'
                                                            id="analyst_email" placeholder="Optional">

                                                        <div class="error-msg err_hide" id="err_analyst_email">
                                                            <br>
                                                        </div>

                                                        <span id="err_email" style="color: red;display: none;">Email Already Exist</span>
                                                    </div>

                                                </div>

                                                <span class="error-msg"></span>
                                            </div>
                                        </div>

                                        <!--email-->
                                        <div class="col-sm-10 analyst_div div_br_btm">
                                            <div class="form-group label-static new-form-group">

                                                <span for="analyst_password" class=""> Password</span>

                                                <div style="">

                                                    <div style="float: left;width: 10%">
                                                        <i id="icon_password_open" style="display:none;" class="input_icon fa fa-eye" aria-hidden="true"></i>
                                                        <i id="icon_password_close" style="display:block;" class="input_icon fa fa-eye-slash" aria-hidden="true"></i>
                                                    </div>

                                                    <div style="float: right;width: 90%">
                                                        <input type="password" class="" name='analyst_password'
                                                            id="analyst_password" placeholder="Optional">

                                                        <div class="error-msg err_hide" id="err_analyst_password">
                                                            <br>Optional
                                                        </div>
                                                    </div>

                                                </div>

                                                <span class="error-msg"></span>
                                            </div>
                                        </div>


                                        <script
                                            src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js">
                                        </script>
                                        <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css"
                                            rel="stylesheet" />

                                        <!--skills-->
                                        <div class="col-sm-10 analyst_div div_br_btm">
                                            <div class="form-group label-static new-form-group">

                                                <span for="analyst_name" class=""> Skills</span>

                                                <div style="">

                                                    <div style="float: left;width: 10%">
                                                        <img class="icon_img"
                                                            src="<?php echo $url;?>uploads/Analyst_img/Skills.png">
                                                    </div>

                                                    <div style="float: right;width: 90%;border: 0px !important;">

                                                        <select data-placeholder="Enter Skills" multiple
                                                            class="chosen-select" name="analyst_skills[]"
                                                            id="analyst_skills">
                                                            <?php

                                                            $skills_query = "SELECT * FROM skills";
                                                            $skills_details = mysqli_query($dbh, $skills_query);

                                                            while($row = mysqli_fetch_array($skills_details))
                                                            { 
                                                                    $id = $row['id'];
                                                                    $name = $row['name'];

                                                                ?> <option value="<?php echo $name; ?>">
                                                                <?php echo $name; ?></option>
                                                            <?php
                                                            }

                                                        ?>
                                                        </select>

                                                    </div>
                                                </div>

                                                <span class="error-msg"></span>
                                            </div>
                                        </div>

                                        <!--exp-->
                                        <div class="col-sm-10 analyst_div div_br_btm">
                                            <div class="form-group label-static new-form-group">

                                                <span for="analyst_exp" class=""> Year of
                                                    experience</span>

                                                <div style="">

                                                    <div style="float: left;width: 10%">
                                                        <img class="icon_img"
                                                            src="<?php echo $url;?>uploads/Analyst_img/Experience.png">
                                                    </div>

                                                    <div style="float: right;width: 90%">
                                                        <!-- <input type="text" class="form-control" name='analyst_exp' id="analyst_exp" required placeholder="Enter Year of experience"> -->


                                                        <select data-placeholder="Select Year of experience"
                                                            class="chosen-select" name="analyst_exp" id="analyst_exp">

                                                            <?php
                                                            for($i=0; $i<=30;$i++)
                                                            {
                                                                ?>
                                                            <option <?php echo $i; ?>><?php echo $i; ?></option>
                                                            <?php
                                                            }
                                                            ?>

                                                        </select>

                                                    </div>
                                                </div>

                                                <span class="error-msg"></span>
                                            </div>
                                        </div>



                                        <!--location-->
                                        <div class="col-sm-10 analyst_div div_br_btm">
                                            <div class="form-group label-static new-form-group">

                                                <span for="analyst_location" class=""> Location</span>

                                                <div style="">

                                                    <div style="float: left;width: 10%">

                                                        <img class="icon_img"
                                                            src="<?php echo $url;?>uploads/Analyst_img/Location.png">
                                                    </div>

                                                    <div style="float: right;width: 90%">

                                                        <input type="text" class="" name='analyst_location'
                                                            id="analyst_location" required placeholder="Enter Location">
                                                            <input type="hidden" name="lat" class="" id="lat">
                                                         <input type="hidden" name="long" id="long">

                                                    

                                                    </div>
                                                </div>

                                                <span class="error-msg"></span>
                                            </div>
                                        </div>

                                        <!--resume-->
                                        <div class="col-sm-10 analyst_div">
                                            <div class="form-group label-static new-form-group">

                                                <span for="analyst_resume" class=""> Add Resume</span>

                                                <div class="div_attch_img">

                                                    <input type="file" name="fileToUpload" id="fileToUpload">
                                                    <img src="<?php echo $url;?>uploads/Analyst_img/Attach.png"
                                                        style="width:15px;">
                                                </div>

                                                <br>

                                                <span id="show_file_name" class=""></span>


                                                <span class="error-msg"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer" style="border-radius: 10px;">
                                        <center>
                                            <input type="submit" class="btn btn-add-analyst button-color openmodale"
                                                style="color: white !important;border-radius: 5px; width: 100px; font-size: initial;        font-family: 'Lato', sans-serif !important; margin-bottom: 50px;"
                                                id="btn_add_analyst" name="save_analyst" value="SAVE">
                                        </center>
                                    </div>

                                </div>

                            </form>

                        </div>
                        <!-- /Modal -->

                        <div class="col-md-12" style="padding: 0px;">

                            <div class="col-md-8 prl-40">
                                <div class="box-white clearfix"
                                    style="box-shadow: 0 1px 6px 0 rgb(0 0 0 / 12%), 0 1px 6px 0 rgb(0 0 0 / 12%); padding: 20px 0px;min-height: 100vh;">
                                    <div class="col-md-12">

                                        <style type="text/css">
                                        .th {
                                            color: #004fff;
                                        }
                                        </style>

                                        <center class="div_header">
                                            <div class="col-md-12" style="margin-bottom: 10px;">
                                                <h2>Expert Details

                                                    <div class="col-md-12 txt-black" style="margin-bottom: 20px;font-weight: bold;  margin-top: -30px;">
                                                <span>
                                                    <?php 
                                                        //echo "<pre>"; print_r(mysqli_num_rows($analyst_details)); echo "</pre>";

                                                    if(mysqli_num_rows($analyst_details) == 0 || mysqli_num_rows($analyst_details) <= 5) { ?>
                                                        <a href="add_analyst.php">

                                                            <button class="btn btn-add-analyst button-color openmodale"
                                                                style="float: right;color: white !important;border-radius: 5px; font-size: initial; padding: 6px 20px;background: #ecc41e !important;">
                                                                <i class="fa fa-plus" aria-hidden="true"
                                                                    style="color: white !important;margin-top: 5px;font-size: small;padding-left: 10px;"></i>&nbsp;&nbsp;
                                                                Add Expert
                                                            </button>
                                                        </a>
                                                        <?php }elseif(mysqli_num_rows($analyst_details) == 0)
                                                    {
                                                    ?>
                                                        <button class="btn btn-add-analyst button-colo openmodaler"
                                                            style="float: right;color: white !important;border-radius: 5px; font-size: initial; padding: 6px 20px;background: #ecc41e !important;">
                                                            <i class="fa fa-plus" aria-hidden="true"
                                                                style="color: white !important;margin-top: 5px;font-size: small;padding-left: 10px;"></i>&nbsp;&nbsp;
                                                            Add Expert
                                                        </button>
                                                        <?php
                                                    } ?>
                                                   
                                                </span>
                                            </div></h2>
                                                <div style="height: 2px;width: 240px;background-color: #ecc41e;"></div>

                                                <br><br>
                                            </div>
                                        </center>

                                        <?php if($_SESSION['agency_id'] != '') { ?>
                                        <div class="col-md-12">
                                            

                                        </div>
                                        <?php } else { ?>
                                        <div class="col-sm-10">
                                            <center>
                                                <div
                                                    style="font-size: 20px;font-weight: bold;padding-top: 30px;padding-bottom: 10px;color: #FF0000">
                                                    Sorry Your Agency is not activated.
                                                </div>
                                            </center>

                                            <center><a href='freelance-profile.php' style="background:#ecc41e;"
                                                    class='btn btn-primary'>OK</a></center>

                                        </div>
                                        <?php } ?>

                                        <?php
                                            if(mysqli_num_rows($analyst_details) > 0) 
                                            {
                                        ?>
                                        <div style="padding: 0px 20px;">
                                            <table class='table' style="border: 1px solid #dddddd;width: 100%;">
                                                <thead style="background-color: #80808021;">
                                                    <tr class="text-center txt-black">
                                                        <td style="color: #4f8edc;text-align: left;width:20%">NAME</td>
                                                        <td style="color: #4f8edc;text-align: left;width:20%">EMAIL ID</td>   
                                                        <td style="color: #4f8edc;text-align: left;width:20%">LOCATION</td>
                                                        <td style="color: #4f8edc;text-align: left;width:20%">VIEW</td>
                                                        <td style="color: #4f8edc;text-align: left;width:20%">DETACH</td>
                                                    </tr>
                                                </thead>
                                                <tbody class="txt-black">
                                                    <?php

                                                while($row = mysqli_fetch_array($analyst_details)) {

                                                   //echo "<pre>"; print_r($row); echo "</pre>";

                                                 ?>
                                                    <tr class="text-center">
                                                        <td style="text-align: left;width:20%"><?= $row["name"] ?></td>
                                                        <td style="text-align: left;width:20%"><?= $row["email_id"] ?></td>
                                                        <td style="text-align: left;width:20%"><?= $row["location"] ?></td>


                                                        <td style="text-align: left;width:20%"><?php 
                                                            if($row["resume"] !='')
                                                            {
                                                                ?>   
                                                                <a target="_blank" href="<?php echo $url.''.$row["resume"]; ?>"><i class="fa fa-eye" aria-hidden="true"
                                                                    style="color: #ecc41e;"></i></a>
                                                                <?php
                                                            }
                                                            else
                                                            {
                                                                ?>
                                                                N/A
                                                                <?php
                                                            }
                                                        ?>
                                                        </td>
                                                       

                                                        <td style="text-align: left;width:20%"><a href="analyst_control.php?mode=d&id=<?= $row["id"] ?>">
                                                                <!-- <i  class="fa fa-link" aria-hidden="true" style="color: red;"></i> -->
                                                                <img src="images/link-icon.png" style="width: 15px;">
                                                            </a></td>
                                                    </tr>
                                                    <?php } ?>

                                                </tbody>
                                            </table>
                                        </div>

                                        <?php 
                                            }
                                            else
                                            {
                                                ?>
                                        <center>
                                            <p>Add your first expert.</p>
                                        </center>
                                        <?php
                                            }

                                        ?>
                                    </div>
                                </div>


                            </div>

                            <br>

                            <style>
                            #div_how_it_works:hover
                            {
                                /*height: 1000px;
                                width: 1000px;*/
                            }
                            </style>

                            <div class="col-md-4" style="padding: 0;" id="div_how_it_works">

                                <div class="box-white clearfix" style="padding: 30px 30px 10px 20px;">
                                    <center class="div_header" style="margin-top: -50px;">
                                        <div class="col-md-12" style="margin-bottom: 10px;margin-top: 20px;">
                                            <h3>How It Works</h3>
                                            <div style="height: 2px;width: 200px;background-color: #ecc41e;"></div>
                                            <br><br>
                                        </div>
                                    </center>

                                    <style>
                                    a {
                                        color: #666666;
                                    }

                                    a:hover {
                                        color: #ecc41e;
                                    }

                                    @media (min-width : 360px) and (max-width : 600px) {
                                        span.es-joblist-info-text.es-block {
                                            word-break: break-word;
                                        }

                                        .pluse_icon {
                                            float: right;
                                            font-weight: 800;
                                            font-size: 18px;
                                            color: #666666;
                                            position: absolute;
                                            right: 5rem;
                                        }

                                        .es-work-block {
                                            width: 90%;
                                            margin: 12px auto;
                                            right: unset;
                                            position: unset;
                                        }

                                        .es-load-width {
                                            width: 59%;
                                        }

                                        .es-faq-block {
                                            top: unset;
                                            margin: 12px auto;
                                            width: 90%;
                                            right: unset;
                                            position: unset;
                                        }

                                        .es-padd-box {
                                            padding: 20px 40px;
                                        }

                                        .es_col_md_1_2.es_col_md_layout.emailinput {
                                            width: 100%;
                                        }

                                        img.es-job-resume-upload-img {
                                            height: 50px;
                                        }

                                        .es-admin-page .ace-responsive-menu>li>a {
                                            padding: 10px 12px 0px 12px;
                                            font-weight: 400;
                                        }

                                        ul#respMenu {
                                            margin-top: 20px;
                                        }

                                        a.es-lg-modal-opener {
                                            display: none;
                                        }

                                        .es-header-2 {
                                            padding-top: 20px;
                                        }

                                        .es-section-browse-job-filter {
                                            margin: 0px;
                                        }

                                        .es-row-admin {
                                            position: relative;
                                            width: 90% !important;
                                            margin: 12px auto !important;
                                        }

                                        .es-row-admin.es-job-row-main.es-clearfix.job_box_load {
                                            margin-left: unset;
                                        }

                                        .es-job-filter-box {
                                            background-color: #f4f5f5;
                                            padding: 25px 20px;
                                            border-radius: 10px;
                                        }

                                        .es-filter-search-btn {
                                            position: absolute;
                                            height: 49px;
                                            align-items: center;
                                            display: flex;
                                            text-align: center;
                                            margin-left: 0px;
                                        }

                                        .es-job-filter-input {
                                            padding: 15px 5px 15px 40px;
                                        }

                                        .es-filter-search-btn img {
                                            display: block;
                                            margin: 0 auto;
                                            width: 20px;
                                        }

                                        img {
                                            width: 80%;
                                        }

                                        .es-admin-page .menu-toggle #menu-btn {
                                            margin: 0px 10px 0px 10px;
                                        }

                                        /* .es-header-2 {
                                     padding-top: 10px;
                                     } */
                                        .es-menu-container {
                                            margin-top: -28px;
                                        }

                                        .es-container-row.es-clearfix {
                                            margin: 15px auto;
                                        }

                                        .es-admin-page.es-menu-container {
                                            top: -1px;
                                        }

                                        .es-load-jobs {
                                            margin-top: 25px;
                                        }

                                        ul.es-social-icons {
                                            top: -10px;
                                        }

                                        .es-joblist-box {
                                            /*padding: 10px 20px;*/
                                            padding: 2rem 3rem 1rem;
                                        }

                                        /* for footer */
                                        .es-footer {
                                            border-radius: 100%/15px 15px 0 0;
                                        }

                                        .es-footer-main {
                                            width: 88%;
                                        }

                                        footer.es-section.es-footer {
                                            height: 420px;
                                        }

                                        h3.es-widget-title.contact {
                                            margin-top: 70px;
                                        }

                                        h3.es-widget-title {
                                            margin-top: 30px;
                                            font-size: 16px;
                                        }

                                        #es_widget_2 {
                                            display: none;
                                        }

                                        div#es_widget_5 {
                                            float: right;
                                            width: 50%;
                                            margin-top: -260px !important;
                                            text-align: center;
                                            border-bottom: 0px !important;
                                        }

                                        div#es_widget_4 {
                                            float: left;
                                            width: 50%;
                                            margin-bottom: 30px;
                                            border-bottom: 0px !important;
                                        }

                                        div#es_widget_3 {
                                            float: left;
                                            width: 51%;
                                            margin-top: -45px !important;
                                            margin-bottom: 30px;
                                            border-bottom: 0px !important;
                                        }

                                        div#es_widget_1 {
                                            float: left;
                                            width: 60%;
                                            margin-bottom: 30px;
                                            border-bottom: 0px !important;
                                        }

                                        p {
                                            line-height: 1.5;
                                            font-size: 15px !important;
                                            letter-spacing: 2px;
                                        }

                                        .es-footer {
                                            border-radius: 100%/15px 15px 0 0;
                                        }

                                        h3.es-widget-title {
                                            font-size: 18px;
                                            margin-top: 35px;
                                        }

                                        .es-footer-main {
                                            padding: 10px;
                                            margin-top: -70px;
                                        }

                                        div#es_widget_5 {
                                            margin-top: -215px;
                                            text-align: left;
                                        }

                                        /* New Class Added "contact-para" to Contact Us paragraph*/
                                        .contact-para {
                                            padding-left: 20px;
                                        }

                                        /* New Class Added "contact" to Contact Us Widget Title */
                                        h3.es-widget-title.contact {
                                            padding-left: 19px;
                                            padding-bottom: 20px;
                                        }

                                        ul li a {
                                            font-size: 16px !important;
                                            letter-spacing: 2px;
                                        }

                                        .es-footer-bottom {
                                            background-color: rgba(84, 93, 93, 3);
                                            padding: 0 0;
                                            margin-top: -5px;
                                        }

                                        .es_row_fullwidth_fields.es-clearfix {
                                            padding-bottom: 20px;
                                        }

                                        div#es_widget_4 {
                                            margin-top: -28px;
                                        }

                                        .es-dashboard-left.es-dashboard-col {
                                            display: none;
                                        }

                                        .es-copyright {
                                            display: block;
                                            font-size: 12px;
                                            font-weight: 300;
                                            margin-bottom: 5px;
                                        }

                                        .es-policy {
                                            font-size: 12px;
                                            font-weight: 600;
                                        }

                                        .es-container-row.es-clearfix {
                                            margin: 15px auto;
                                        }

                                        .es-row-fullwidth.es-clearfix {
                                            padding: 10px 0;
                                        }

                                        /* Display Jobs Section CSS Start */
                                        .es-row-full-inside.es-clearfix {
                                            display: flex;
                                            flex-wrap: wrap;
                                            justify-content: flex-start;
                                        }

                                        .es_col_1_5.es_col_layout {
                                            box-sizing: border-box;
                                            width: 50%;
                                            border-bottom: 2px solid black;
                                            border-radius: 0;
                                        }

                                        .es_col_layout {
                                            margin-right: unset;
                                            margin: 5px 0;
                                        }

                                        .es_col_1_4.es_col_layout {
                                            box-sizing: border-box;
                                            width: 45%;
                                            border-bottom: 2px solid black;
                                        }

                                        .es-joblist-bid {
                                            text-align: center;
                                        }

                                        .es-row-full-inside.es-clearfix.es-row-job-id-status-box.es-fill-color-ecc41e.es-border-radius-8x {
                                            background-color: white;
                                            border: 2px solid orange;
                                        }

                                        .es-fill-color-ecc41e span {
                                            color: orange;
                                        }

                                        .es-fill-color-ecc41e .es-row-job-id-status:first-child {
                                            border-bottom: 2px solid orange;
                                        }

                                        /* Display Jobs Section CSS End */
                                    }

                                    .mr-0 {
                                        margin-right: unset !important;
                                    }

                                    .m-0 {
                                        margin: unset !important;
                                    }

                                    .yellow-btn {
                                        background-color: #fff;
                                        border: 2px solid #ecc42e;
                                        color: #ecc42e;
                                    }

                                    .es-btn-default:hover {
                                        background-color: #fff;
                                        border: 2px solid #ecc42e;
                                        color: #ecc42e;
                                    }

                                    .y_text {
                                        margin-bottom: 15px;
                                        min-height: 35px;
                                        top: -27px;
                                        position: relative;
                                        font-size: 14px;
                                        margin-left: 26px;
                                        display: inline-table;
                                        letter-spacing: .3px;
                                        color: #666666;
                                        font-weight: 500;
                                        font-family: "Lato";
                                    }

                                    .y_text:hover {
                                        color: #ecc42e !important;
                                        font-size: 14.5px;
                                    }

                                    .bar {
                                        list-style: none;
                                    }

                                    .bar>li {
                                        position: relative;
                                    }

                                    .bar>li:before {
                                        /*content: '\f192';
                                       font-family: "Font Awesome 5 Free" !important;
                                       margin-right: 10px;
                                       font-size: 20px;
                                       color: #ecc41e;*/
                                        content: "";
                                        display: inline-block;
                                        width: 20px;
                                        height: 20px;
                                        padding: 2px;
                                        margin-right: 5px;
                                        background-clip: content-box;
                                        border: 2px solid #ecc41e;
                                        background-color: #ecc41e;
                                        border-radius: 15px;
                                    }

                                    .bar>li:after {
                                        position: absolute;
                                        left: 0;
                                        top: 50%;
                                        content: '';
                                        border-left: 3px dotted #666666;
                                        margin-left: 8px;
                                        height: 25%;
                                    }

                                    .bar>li:first-of-type:after {
                                        top: 50%;
                                    }

                                    .bar>li:last-of-type:after {
                                        display: none;
                                        top: -50%;
                                    }

                                    .es_col_filter:nth-child(1) {
                                        width: 11.5%;
                                        margin-right: 0.1%;
                                    }

                                    .es_col_filter:nth-child(2) {
                                        width: 67%;
                                        margin-right: 1%;
                                    }

                                    .refresh_button {
                                        margin-top: -51px;
                                        padding: 11px 40px;
                                        float: left;
                                        margin-left: 146px;
                                    }
                                    </style>

                                    <div class="es-box-shadow es-border-radius-8x es-work-block"
                                        style="background-color: #f2f2f2; box-shadow: 0px !important;">
                                        <div class="es-padd-box" style="padding: 3rem 3rem 2rem 4rem;">
                                            <div class="es-how-work" style="height: 65px;">&nbsp;</div>
                                            <ul class="bar">
                                                <li>
                                                    <div class="y_text"> Make a bid on a job that suits your profile &
                                                        availability.</div>
                                                </li>
                                                <li>
                                                    <div class="y_text"> Sign in or register so we can review your
                                                        details.</div>
                                                </li>
                                                <li>
                                                    <div class="y_text"> You can login to your dashboard & see the
                                                        project on the 'Bid Ongoing' section.</div>
                                                </li>
                                                <li>
                                                    <div class="y_text"> Take a skill test so that a company can assess
                                                        your skills.</div>
                                                </li>
                                                <li>
                                                    <div class="y_text"> If your profile is shortlisted, you will be
                                                        notified for an interview.</div>
                                                </li>
                                                <li>
                                                    <div class="y_text"> Once you get selected, you will be invited to
                                                        submit a detailed project scope.</div>
                                                </li>
                                            </ul>
                                        </div>


                                    </div>


                                    <div class="box-white new-white-box clearfix faq_section">
                                        <center class="div_header" style="margin-top: -40px;">
                                            <div class="col-md-12" style="margin-bottom: 10px;margin-top: 20px;">
                                                <h3>FAQ'S</h3>
                                                <div style="height: 2px;width: 100px;background-color: #ecc41e;"></div>
                                        </center>

                                        <div>

                                            <button class="accordion">
                                                <div>
                                                    <a target="_blank"
                                                        href="https://econolytics.zendesk.com/hc/en-us/articles/360014653793-How-can-I-register-on-Econolytics-">
                                                        <span class="ft-lt section_lable">
                                                            How can I register on Econolytics?
                                                        </span>
                                                    </a>
                                                </div>
                                            </button>

                                            <div class="hr_faq"></div>

                                            <button class="accordion">
                                                <a target="_blank"
                                                    href="https://econolytics.zendesk.com/hc/en-us/articles/360021930474-What-is-the-right-engagement-model-for-me-"><span
                                                        class="ft-lt section_lable">What is the right engagement model
                                                        for me?</span>
                                                </a>
                                            </button>

                                            <div class="hr_faq"></div>


                                            <button class="accordion">
                                                <a target="_blank"
                                                    href="https://econolytics.zendesk.com/hc/en-us/articles/360014653933-How-do-I-post-a-job-"><span
                                                        class="ft-lt section_lable">How do I post a job?</span>
                                                </a>
                                            </button>

                                            <div class="hr_faq"></div>


                                            <button class="accordion">
                                                <a target="_blank"
                                                    href="https://econolytics.zendesk.com/hc/en-us/articles/360048285374-How-does-Econolytics-ensure-Accountability-of-data-experts-working-on-projects-"><span
                                                        class="ft-lt section_lable">How does Econolytics ensure
                                                        accountability of data experts working on projects?</span>
                                                </a>
                                            </button>

                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>

            </section>
        </div>

    </div>
    </div>



    <?php include("profile_footer.php");  ?>

    <script>
    $('.es-dashboard-left').height($(".section_2").height() + 80);
    $('.es-main-menu').height($(".es-dashboard-left").height() + 0);
    </script>

    <script type="text/javascript">
    $(".chosen-select").chosen({
        no_results_text: "Oops, nothing found!"
    });

    $('input[type="file"]').change(function(e) {
        var file_name = e.target.files[0].name;
        //alert(file_name);    
        $("#show_file_name").html(file_name);
    });

    $("#icon_password_close").click(function() {
        $("#icon_password_open").show();
        $("#icon_password_close").hide();

        $("#analyst_password").attr("type", "Password");
    });

    $("#icon_password_open").click(function() {
        $("#icon_password_open").hide();
        $("#icon_password_close").show();

        $("#analyst_password").attr("type", "text");
    });

    $("#analyst_name").keyup(function(event) {

        $("#err_analyst_name").hide();

        let analyst_name = $("#analyst_name").val();

        let = name_lenght = analyst_name.length;

        if (name_lenght > 5) {
            analyst_name = analyst_name.substring(0, 5);
        }

        let datetime = <?php echo date("Ymd"); ?>;

        let Password = analyst_name + '@' + datetime;

        $("#analyst_password").val(Password);
    });

    //You may use vanilla JS, I just chose jquery

    $('.openmodale').click(function(e) {
        e.preventDefault();
        $('.modale').addClass('opened');
        $('.modal-dialog').fadeIn("6000");
        $('.opened:before').css('display', 'block');
    });

    $('.closemodale').click(function(e) {
        e.preventDefault();
        $('.modale').removeClass('opened');
        $('.modal-dialog').fadeOut();
        $('.opened:before').css('display', 'block');
    });


    $("#btn_add_analyst").click(function() {

        flag = 0;
        $(".err_hide").hide();
        $("#err_email").hide();

        //---------------//

        let analyst_name = $("#analyst_name").val();
        //alert(analyst_name);

        let analyst_email = $("#analyst_email").val();
        //alert(analyst_email);

        let analyst_password = $("#analyst_password").val();
        //alert(analyst_password);

        let analyst_skills = $("#analyst_skills").val();
        //console.log(analyst_skills);

        let analyst_exp = $("#analyst_exp").val();
        //alert(analyst_exp);

        let analyst_location = $("#analyst_location").val();
        //alert(analyst_location);

        let analyst_resume = $("#analyst_resume").val();
        //alert(analyst_resume);

        //----------------//

        if (analyst_name == "") {
            flag = 1;
            //alert("Please Enter Analyst Name");

            $("#err_analyst_name").show();
        }

        //----------------//

        var form = $("#add_analyst_form");
        var formData = new FormData(form[0]);

        if (flag == 0) {
            $.ajax({
                url: 'analyst_control_new.php',
                type: 'POST',
                data: formData,
                success: function(res) {

                    //alert(res);
                    var res = $.trim(res);

                    if(res == "exist")
                    {
                        //alert("hi");
                        $("#err_email").show();
                    }
                    else
                    {
                        //alert("bye");
                       window.location.reload();
                    }


                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

    });

    $('#closed_popup').click(function() {
        $('.modale').removeClass('opened');
        $('.modal-dialog').fadeOut();
        $('.opened:before').css('display', 'block');
    })
    </script>

    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', initialize);
function initialize() {
    var input = document.getElementById('analyst_location');
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
        console.log(place.geometry['location'].lat());
        // place variable will have all the information you are looking for.
        $('#lat').val(place.geometry['location'].lat());
        $('#long').val(place.geometry['location'].lng());
    });
}
    </script>

</body>