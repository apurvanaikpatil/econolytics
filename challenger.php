<?php

session_start();
include("include/config.inc.php");
include("include/head.php");

if( $_SESSION['freelance_email']=='' && $_SESSION['user_info']['emailAddress']=='') {
  echo "<script>document.location.href='index.php';</script>";
  exit;
}

//Getting Parameters cleaning it and store it in variables
$challenge_id = mysqli_real_escape_string($dbh,$_GET["challenge_id"]);
$challenger = mysqli_real_escape_string($dbh,$_GET["challenger"]);
$analyst_id = mysqli_real_escape_string($dbh,$_GET["analyst_id"]);

// echo "$challenge_id";
//Compare challenge id and analyst id whether they're same if yes, get that challenges row data
$query=mysqli_query($dbh,"SELECT * FROM challenges WHERE challenge_id='$challenge_id' AND employee_id='$analyst_id'");
$challenge=mysqli_fetch_assoc($query);
//print_r($challenge);



//$challenge_type = explode(",",$details['challenge_type']); //Not required as of now
  $question = explode("~~~",$challenge['question']);

  //print_r($question);

//Check if user have already appeared quiz or not
if($challenge['challenge_score'] != "") {
	header("Location:freelance-challenges.php");
	exit;
}

// update into challenge table date and time when link was open
$myquery = mysqli_query($dbh, "UPDATE challenges SET click_email_link_status=1,email_link_click_date=now() WHERE challenge_id='$challenge_id'");

?>
<script type="text/javascript" src="project_management_analyst/js/bootstrap.min.js"></script>

<!-- Some styles to style Forms, Timer and Success Sections Starts Here -->
<style>


.color-full {
        background: #649e20 !important;
}
.challenger p {
    margin-top: 38px !important;
}
.dashboard-box {
    margin-top: 0px;
    padding-bottom: 0px;
}
.heading-box h2
{
    color: white;
    margin:0px;
}
.heading-box {
    padding: 35.5px !important;
}
    .form .table > thead > tr > th {
        line-height: 1 !important;
        padding: 0;
        font-size: 14px;
        color: #000;
    }

    .form .table > tbody > tr > td {
        line-height: 1 !important;
        padding: 0;
        font-size: 14px;
        color: #000;
    }

	.btn-start {
		padding: 10px 24px !important;
	    width: 84px;
	    margin-right: 37px;
	}
	.white-info-box .form-group:last-child {
	    margin-bottom: 12px;
	}
	.option {
	    color: grey;
	}

	.success {
		font-size:30px;
		color:#4CAF50;
	}

	.error {
		font-size:30px;
		color:#da190b;
	}

	.challenger p {
		text-align:center;
		font-size:20px;
		margin-top:120px;
		line-height: 1.5;
	}
	.challenger-completed {
		display : none;
	}

	.challenger-completed p {
		text-align:center;
		font-size:20px;
		margin-top:120px;
		line-height: 1.5;
	}

	.btn-download {
	    font-size:14px;
	    cursor: hand;
	    color: #fff;
	    padding: 5px 5px;
	    background-color: #5bc0de;
	    border-color: #46b8da;
	    text-decoration: none;
	}

	  .btn-download:hover {
	    text-decoration: none;
	    color: #fff;
	    background-color: #0196bf;
	    border-color: #0196bf;
	  }

	  .row {
	  	margin-top:20px;
	  }
	  .form {
	  	padding: 20px;
	  }

	/* Countdown Timer */
	.countdown-label {
		font: thin 15px Arial, sans-serif;
		color: #65584c;
		text-align: center;
		text-transform: uppercase;
		display: inline-block;
	}
	#countdown{
		padding-top: 10px;
		width: 140px;
		box-shadow: 0 1px 2px 0 rgba(1, 1, 1, 0.4);
		text-align: center;
		background: #f1f1f1;
        overflow: hidden;
	}
	#countdown #tiles{
		color: #fff;
		position: relative;
		z-index: 1;
		text-shadow: 1px 1px 0px #ccc;
		display: inline-block;
		font-family: Arial, sans-serif;
		text-align: center;
		font-size: 30px;
		font-weight: thin;
		display: block;
        height: 34px;
        padding-top: 0px;
	}

	.color-half {
		background: #ebc85d;
	}
	.color-empty {
		background: #e5554e;
	}

	#countdown #tiles > span{
		width: 50px;
		max-width: 500px;
		padding: 18px 0;
		position: relative;
	}
	#countdown .labels{
		width: 100%;
		height: 25px;
		text-align: center;
		position: absolute;
		bottom: 8px;
	}
	#countdown .labels li{
		width: 70px;
		font: bold 15px 'Droid Sans', Arial, sans-serif;
		color: #f47321;
		text-shadow: 1px 1px 0px #000;
		text-align: center;
		text-transform: uppercase;
		display: inline-block;
	}
	.timer {
		margin-bottom: 20px;
	}
	.timer_div{
		float: right;
		text-align: -webkit-right;
	}

	@media screen and (max-width:768px){
		.timer_div{
			text-align: -webkit-center !important;
		}
	}
@media only screen and (max-width:375px){
.heading-box h2 {
    font-size: 21px;
    padding-top: 0px;
    margin: 0px !important;
    text-align: center;
}

}
@media only screen and (max-width:320px){
	.navbar-brand {
		width: 72%;
		margin: 7px 11px;
	}
	.icons_social {
		margin: 0 23px !important;
	}
	.form {
		padding: 20px;
	}
	#countdown{
		text-align:center;
	}
}

.loader{
    z-index: 1;
    position: absolute;
    height: 50px;
    width: 50px;
    top: 50%;
    left: 40%;
    display: none;
}

</style>
<!-- Some styles to style Forms, Timer and Success Sections Ends Here -->
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Econolytics</title>
<!--Responsiveness-->
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<!--FontAwesome-->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="project_management_analyst/assets/css/style.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" href="https://use.typekit.net/eup5wsc.css">
<link href="project_management_analyst/assets/css/ace-responsive-menu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

     <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> -->
<!--Extra JS Start-->
<script type="text/javascript" src="project_management_analyst/js/jquery.min.js"></script>
<script type="text/javascript" src="project_management_analyst/js/bootstrap.min.js"></script>
<!--Extra JS End-->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="project_management_analyst/assets/js/jquery.maphilight.min.js"></script>
<!--Scripts-->
<script src="project_management_analyst/assets/js/ace-responsive-menu.js"></script>
<script src="project_management_analyst/assets/js/imageMapResizer.min.js"></script>
<!-- jQuery Modal -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" /> -->
<!--Extra CSS Start-->
<link rel="stylesheet" type="text/css" href="project_management_analyst/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="project_management_analyst/css/style.css">
<?php //include("include-left-pan.php"); ?>
<?php include("new_left_sidebar_challenger.php"); ?>
<body oncontextmenu="return false;">
<!-- Attaching Header -->
<?php //include("include/header.php"); ?>
<!-- .heading-box -->

<div class="inner-content-box1">

<div class="container">
  <!-- for loader -->
 

   <section class="dashboard-box">
      <div class="row">

         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 freelanc-project">
			<div class="job-box ">
			     <div class="box-body">
			     	<!-- Message to challenger -->
			        <div class="challenger">
			          <p>You have total <?php echo array_sum(explode(",",$challenge['time_limit'])); ?> minutes to complete the challenge once you start.<br/> Challenge may have a number of questions with different time limit.<br/>Click start button when you are ready, you cannot pause the game. <br/>Best of Luck !! <br/> <button id="start" class="btn btn-primary start">Start</button></p>
			        </div><!-- Message to challenger -->

                    <!-- Quiz Form -->
                    <div class="form">

                    </div><!-- Quiz Form -->

					<!-- success-message-div -->
					<div class="challenger-completed">
						 <p>Thank You!!</p>
			     	</div><!-- success-message-div -->

			     </div><!-- box-body -->
			  <div class="clearfix"></div>
			</div><!--end of job box-->
         </div><!--Columns to the Right-->
       </div><!-- Row -->
   </section><!-- dashboard-box -->
    </div><!-- container -->
</div><!-- inner-content-box -->

<footer class="es-section es-footer">
		<div class="es-footer-main">
			<div class="es-container">
				<div class="es-container-row es-clearfix">
					<div class="es-row es-clearfix">
						<div id="es_widget_1" class="es_col_layout es_col_1_5">
							<div class="es-logo-footer">
								<div class="es-widget-text">
									<h3 class="es-widget-title"></h3>
									<a href="index.php" class="es-logo-anchor"><img class="es-logo-img" src="images/logo-footer.png" alt=""></a>
								</div>
							</div>
						</div>
						<div id="es_widget_2" class="es_col_layout es_col_1_5">
							<div class="es-widget-text">
								<h3 class="es-widget-title"></h3>
								<p class="es-font-size-12px">Data is the new oil and at EconoLytics, we aim to make the mining easier and affordable for you. We are a purpose driven data analytics marketplace empowering businesses and professional data analysts around the world and help them both grow exponentially.
								</p>
							</div>
						</div>
						<div id="es_widget_3" class="es_col_layout es_col_1_5">
							<div class="es-widget-text">
								<h3 class="es-widget-title">COMPANY</h3>
								<span class="footer-dot">.</span>
								<ul>
									<li><a href="../team_econolytics.php">Our Team</a></li>
									<li><a href="https://econolytics.zendesk.com/hc/en-us">FAQs</a></li>
									<li><a href="../pricing.php">Pricing</a></li>
								</ul>
							</div>
						</div>
						<div id="es_widget_4" class="es_col_layout es_col_1_5">
							<div class="es-widget-text">
								<h3 class="es-widget-title">LEARN MORE</h3>
								<span class="footer-dot">.</span>
								<ul>
									<li><a href="../qscore.php">Q Score</a></li>
									<li><a href="../dispute-resolution-process.php">Dispute Resolution Process</a></li>
								</ul>
							</div>
						</div>
						<div id="es_widget_5" class="es_col_layout es_col_1_5">
							<div class="es-widget-text">
								<h3 class="es-widget-title">CONTACT US</h3>
								<span class="footer-dot">.</span>
								<p>India Office: <br>
								+91 9899112471 <br>
								info@econolytics.in <br>
								Netherlands Office: <br>
								+31 6 303 959 10 <br>
								info@econolytics.nl
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="es-footer-bottom">
			<div class="es-container">
				<div class="es-container-row es-clearfix">
					<div class="es-row es-clearfix">
						<div class="es-row-fullwidth es-clearfix">
							<div class="es_col_layout es_col_fullwidth">
								<span class="es-copyright">© Copyrights 2019 Econolytics. All rights reserved.</span>
								<span class="es-policy"><a href="../privacy-policy.php">Privacy Policy</a> | <a href="../terms-n-conditions.php">Terms 	&amp; Conditions</a></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

<?php// include ("include/footer.php")?>

<?php //include("include/signup.php");  ?>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/material.min.js"></script>
<script src="js/ripples.min.js"></script>
<script src="js/jquery.dropdown.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="js/jquery.sliderTabs.js"></script>
<script src="js/nouislider.js"></script>
<script src="js/bootstrap-rating.js"></script>
<script src="js/plugins.js"></script>

<!-- Main Logic behind Challenger Goes Here Written by Adevole -->


<script>

	//On click of Start Challenge Button Message will disappear, Form and timer will set to Appear.
	$(".start").click(function(){
        analyst_id = <?php echo $analyst_id; ?>;
        challenge_id = <?php echo $challenge_id; ?>;

        //Calling Ajax parsing user data to challenger-completed.php and getting success or errors back
        jQuery.ajax({
          url:"challenger-functions.php",
          data:'i=0'+'&analyst_id='+analyst_id+
                '&challenge_id='+challenge_id,
          type: "POST",
          success: function(data){
                //
                console.log(data);
                $(".challenger").hide();
                $(".form").html(data);
          },
          error: function(data){ alert(data); }
        });
    });

    var timeTaken = new Array();
    var answer = new Array();
    //On Click of Next Button
    $(document).on('click', '.next', function(){

        total_questions = <?php echo count($question); ?>;
        num_question = parseInt($("#i").val())+1;
        i = $("#i").val();

        if(total_questions != num_question) {
            analyst_id = <?php echo $analyst_id; ?>;
            challenge_id = <?php echo $challenge_id; ?>;

            //Converting hours in to minute adding it to remaining minutes
            hours_to_minute = parseInt(hours)*60;
            minute = parseInt(minutes) + hours_to_minute;
            var time_limit = $('#set-time').val(); //Getting Time Limits of Challenger in Minutes

            //Calculating Difference between time limit provided to user vs time taken by user
            diff_minutes = parseInt(time_limit) - minute;
            diff_seconds = 60 - parseInt(seconds);

            //Setting values in variables
            timeTaken[i] = (diff_minutes - 1)+" Minute "+diff_seconds+" Seconds";
            if(!(document.querySelector('input[name = "option"]:checked'))) {
                 $('.error-text').html('<p class="text-center" style="color:red;">You can not give up, please try again !!</p>');
                return false;
            }
            answer[i] = document.querySelector('input[name = "option"]:checked').id;

            i = parseInt($("#i").val())+1;

            //Calling Ajax parsing user data to challenger-completed.php and getting success or errors back
            jQuery.ajax({
              url:"challenger-functions.php",
              data:'i='+i+'&analyst_id='+analyst_id+
                    '&challenge_id='+challenge_id,
              type: "POST",
              success: function(data){
                    //
                    console.log(data);
                    $(".form").html(data);
              },
              error: function(data){ alert(data); }
            });
        } else {

            //Converting hours in to minute adding it to remaining minutes
            hours_to_minute = parseInt(hours)*60;
            minute = parseInt(minutes) + hours_to_minute;
            var time_limit = $( '#set-time' ).val(); //Getting Time Limits of Challenger in Minutes

            //Calculating Difference between time limit provided to user vs time taken by user
            diff_minutes = parseInt(time_limit) - minute;
            diff_seconds = 60 - parseInt(seconds);

            //Setting values in variables
            var challenge_id = "<?php echo $challenge_id ?>";
            timeTaken[i] = (diff_minutes - 1)+" Minute "+diff_seconds+" Seconds";
            if(!(document.querySelector('input[name = "option"]:checked'))) {
                 $('.error-text').html('<p class="text-center" style="color:red;">You can not give up, please try again !!</p>').fadeout(2000);
                return false;
            }
            answer[i] = document.querySelector('input[name = "option"]:checked').getAttribute("id");
            // console.log(answer);
            // debugger;

            //Calling Ajax parsing user data to challenger-completed.php and getting success or errors back
            // console.log("show loade");
            $(".loader").show();
            jQuery.ajax({
              url:"challenger-functions.php",
              data:'challengeId='+challenge_id+
                    '&answers='+answer+
                    '&timeTaken='+timeTaken,
              type: "POST",
              success: function(data){
                    //Hiding form and timer and showing success message with answer and redirecting user to list of challenges
                    // console.log(data);
                    $(".form").hide();
                    $(".challenger-completed").show();
                    $(".challenger-completed").html(data);
                    // window.setTimeout(function() {
                    //     window.location.href = 'http://econolytics.in/freelance-challenges.php';
                    // }, 5000);
              },
              complete: function(){
                $(".loader").hide();
              },
              error: function(){}
            });
        }
    });

</script>
<!-- Main Logic behind Challenger Goes Here Written by Adevole -->


<script type="text/javascript">

    document.onkeydown = function(e){
        //keycode for F5 function
        if(e.keyCode===116){
          return false;
        }
        //keycode for backspace
        if(e.keyCode===8){
          return false;
         }
        if(e.keyCode===9){
          return false;
        }
        if(e.keyCode===18){
          return false;
        }
        if(e.keyCode===123){
        	return false;
        }
    };
</script>
<script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
</body>
</html>