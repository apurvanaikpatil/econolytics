<?php
    session_start();
    include("include/config.inc.php");
     include("head.php");
?>
<style type="text/css">
    #set-top
    {
         width: 500px;
  height: 324px;
  padding: 84px;
  background-color: #f0fff0;
  box-shadow: 10px 10px;
  border: solid 1px;
  /*color: #a00;*/
    }


</style>
<body>
    <?php
    include("header.php");
    ?>
    <div class="heading-box set-box">
        <div class="container">
            <h2>Reset Password</h2>
        </div>
    </div>
    <div class="inner-content-box1 polaroid">
        <div class="container">
            <section class="dashboard-box" >
                <div class="row row-set">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    </div>
                    <?php $email1=$_REQUEST['attached_mail']; ?>
                    <?php $email2=$_REQUEST['employer_email']; ?>
                    <?php
                            if(isset($_REQUEST['attached_mail'])){  ?>

                               <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="margin-left: -405px;">
                                    <div id="set-top">
                                        <?php if($_REQUEST['msg_client']==4) {  ?>
                                        <div style="color:green" id="successMessage">Your password has been reset successfully. <br/>Redirecting you to Homepage..</div>
                                        <?php
                                        // header('url=client-profile-edit.php');
                                        header( "refresh:3;url=project_management/bid-ongoing.php" );
                                         }
                                        ?>
                                        <center><h3>Enter a New Password</h3></center>
                                        <form action="attached-change-password-process.php" method="post" >
                                            <input type="hidden" name="attached_email" value="<?php echo $email1; ?>">
                                            <div class="form-group label-static">
                                                <br>
                                                <input type="password" name="attached_password" placeholder="Password" class="control-label password"  id="attached_password">
                                            </div>
                                            <div class="form-group label-static">
                                                <input type="password" name="cpass" placeholder="Confirm Password" class="control-label password" value="" id="attached_confirm_password">
                                            </div>
                                            <div class="form-group label-static">
                                               <center> <input type="submit" name="attached_change_password" class="btn btn-primary button-color" value="Submit"></center>
                                            </div>
                                        </form>
                                    </div>
                                </div>





                       <?php   } ?>



                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"></div>
                </div>
            </section>
        </div>
    </div>
    <?php include("footer.php"); ?>
    <?php //include("include/signup.php"); ?>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/material.min.js"></script>
    <script src="js/ripples.min.js"></script>
    <script src="js/jquery.dropdown.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.sliderTabs.js"></script>
    <script src="js/nouislider.js"></script>
    <script src="js/bootstrap-rating.js"></script>
    <script src="js/plugins.js"></script>
    <script>
        var password = document.getElementById("password");
        confirm_password = document.getElementById("confirm_password");
        function validatePassword() {
            if(password.value != confirm_password.value) {
                confirm_password.setCustomValidity("Passwords Don't Match");
            } else {
                confirm_password.setCustomValidity('');
            }
        }
        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;
    </script>
    <script>
        setTimeout(function() {
          $('#successMessage').fadeOut('fast');
          //window.location.href = "index.php";
        }, 3000); // <-- time in milliseconds
    </script>

    <script>
        var password = document.getElementById("client_password");
        confirm_password = document.getElementById("client_confirm_password");
        function validatePassword_client() {
            if(client_password.value != client_confirm_password.value) {
                client_confirm_password.setCustomValidity("Passwords Don't Match");
            } else {
                client_confirm_password.setCustomValidity('');
            }
        }
        client_password.onchange = validatePassword_client;
        client_confirm_password.onkeyup = validatePassword_client;
    </script>




</body>
</html>