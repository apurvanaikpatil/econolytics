<?php 

session_start();
include_once("include/config.inc.php");
include_once('ecomail/mailer/send_mail.php');
date_default_timezone_set("Asia/Calcutta"); 
$user_id = $_SESSION['admin_id'];
$id= $_GET['id'];
$job_id= $_GET['job_id'];
$type= $_GET['type'];
$emailID= $_GET['email_id'];

if($type == 'hired'){
	$sql_1 = "UPDATE `analyst_job_match_details` SET `ats_approve_flag` = '1', `more_status` = 'HIRED BY CLIENT',`updated_at`=CURRENT_TIMESTAMP     WHERE 	`analyst_id` = $id AND `job_id` = $job_id";

	mysqli_query($dbh,$sql_1);  

	$select_employee = "SELECT `email_id`, `name` FROM `employee` WHERE `id` =  $id";
	$res = mysqli_query($dbh, $select_employee);
	    $num = mysqli_fetch_assoc($res);
	    $email_id = $num['email_id'];
	    $analyst_name = $num['name'];

	    if($analyst_name ==""){

	    		$select_employee = "SELECT `email_id`, `name` FROM `newly_added_analysts` WHERE `email_id` =  '$emailID'";
	$res = mysqli_query($dbh, $select_employee);
	    $num = mysqli_fetch_assoc($res);
	    // $email_id = $num['email_id'];
	    $analyst_name = $num['name'];

	    }


	$sql_2 = "UPDATE `newly_added_analysts` SET  `ats_approved_flag` = '1',`more_status` = 'HIRED BY CLIENT',`updated_at`=CURRENT_TIMESTAMP     WHERE 	`email_id` = '$emailID' AND `job_id` = $job_id";

	mysqli_query($dbh,$sql_2); 



	$sql = "UPDATE `eco_applied_jobs` SET `ats_approve_flag` = '1', `more_status` = 'HIRED BY CLIENT', `updated_at`=CURRENT_TIMESTAMP    WHERE `employee_id_fk` = $id AND 	job_id_fk = $job_id";

	mysqli_query($dbh,$sql); 


	$time= date('Y/m/d H:i:s', time());

	echo $sql_insert_action = "INSERT INTO `candidate_application_logs` (`user_id`, `candidate_id`, `action`, `created_at`) VALUES ('$user_id', '$id', 'Hired by Client', '$time')";
	mysqli_query($dbh,$sql_insert_action);
}
elseif ($type == 'shortlisted') {
	$sql_1 = "UPDATE `analyst_job_match_details` SET `ats_approve_flag` = '1', `more_status` = 'SHORTLISTED BY CLIENT', `updated_at`=CURRENT_TIMESTAMP     WHERE 	`analyst_id` = $id AND `job_id` = $job_id";

	mysqli_query($dbh,$sql_1);  

$select_employee = "SELECT `email_id`, `name` FROM `employee` WHERE `id` =  $id";
	$res = mysqli_query($dbh, $select_employee);
	    $num = mysqli_fetch_assoc($res);
	    $email_id = $num['email_id'];
	    $analyst_name = $num['name'];

	    if($analyst_name ==""){

	    		$select_employee = "SELECT `email_id`, `name` FROM `newly_added_analysts` WHERE `email_id` =  '$emailID'";
	$res = mysqli_query($dbh, $select_employee);
	    $num = mysqli_fetch_assoc($res);
	    // $email_id = $num['email_id'];
	    $analyst_name = $num['name'];

	    }


	echo $sql_2 = "UPDATE `newly_added_analysts` SET  `ats_approved_flag` = '1',`more_status` = 'SHORTLISTED BY CLIENT', `updated_at`=CURRENT_TIMESTAMP     WHERE 	`email_id` = '$emailID' AND `job_id` = $job_id";

	mysqli_query($dbh,$sql_2); 



	$sql = "UPDATE `eco_applied_jobs` SET `ats_approve_flag` = '1', `more_status` = 'SHORTLISTED BY CLIENT', `updated_at`=CURRENT_TIMESTAMP    WHERE `employee_id_fk` = $id AND 	job_id_fk = $job_id";

	mysqli_query($dbh,$sql); 


	$time= date('Y/m/d H:i:s', time());

	echo $sql_insert_action = "INSERT INTO `candidate_application_logs` (`user_id`, `candidate_id`, `action`, `created_at`) VALUES ('$user_id', '$id', 'Shortlisted by Client', '$time')";
	mysqli_query($dbh,$sql_insert_action);	


	 $to = $emailID;

	  $to_name = $analyst_name;

	  $params = array(
            
            'analyst_name' => $analyst_name,
             'job_id' => $job_id
        );

$attachment_file="";

$user_indicator_analyst = 0;

 $mailStatus_analyst = create_n_send(204, $params, $to, $to_name, $dbh, $attachment_file, $user_indicator_analyst);
}
elseif ($type == 'rejected') {
	$sql_1 = "UPDATE `analyst_job_match_details` SET `ats_approve_flag` = '1', `more_status` = 'REJECTED BY CLIENT', `updated_at`=CURRENT_TIMESTAMP     WHERE 	`analyst_id` = $id AND `job_id` = $job_id";

	mysqli_query($dbh,$sql_1);  

	$select_employee = "SELECT `email_id`, `name` FROM `employee` WHERE `id` =  $id";

	$res = mysqli_query($dbh, $select_employee);
	    $num = mysqli_fetch_assoc($res);
	    $email_id = $num['email_id'];
	    $analyst_name = $num['name'];


	echo $sql_2 = "UPDATE `newly_added_analysts` SET  `ats_approved_flag` = '1',`more_status` = 'REJECTED BY CLIENT', `updated_at`=CURRENT_TIMESTAMP     WHERE 	`email_id` = '$emailID' AND `job_id` = $job_id";

	mysqli_query($dbh,$sql_2); 



	echo $sql = "UPDATE `eco_applied_jobs` SET `ats_approve_flag` = '1', `more_status` = 'REJECTED BY CLIENT', `updated_at`=CURRENT_TIMESTAMP    WHERE `employee_id_fk` = $id AND 	job_id_fk = $job_id";

	mysqli_query($dbh,$sql); 


	$time= date('Y/m/d H:i:s', time());

	echo $sql_insert_action = "INSERT INTO `candidate_application_logs` (`user_id`, `candidate_id`, `action`, `created_at`) VALUES ('$user_id', '$id', 'Rejected by Client', '$time')";
	mysqli_query($dbh,$sql_insert_action);


	  $to = $emailID;

	  $to_name = $analyst_name;

	  $params = array(
            
            'analyst_name' => $analyst_name,
             'job_id' => $job_id
        );

$attachment_file="";

$user_indicator_analyst = 0;

 $mailStatus_analyst = create_n_send(203, $params, $to, $to_name, $dbh, $attachment_file, $user_indicator_analyst);


}
 

?>
