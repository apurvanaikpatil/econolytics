<?php
session_start();
include_once "include/config.inc.php";
include "head.php";
include "header.php";
?>
<!-- chipt new -->
<link href="chip/css/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="chip/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<!-- chipt new -->
<!-- Date Picker -->
<link rel="stylesheet" href="zebra_datepicker/css/default.css" type="text/css">
<link rel="stylesheet" href="zebra_datepicker/css/style.css" type="text/css">
<script type="text/javascript" src="zebra_datepicker/javascript/zebra_datepicker.js"></script>
<script type="text/javascript" src="zebra_datepicker/javascript/core.js"></script>
<!-- Date Picker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="css/on-off-switch.css" />
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="css/freelance-create-account.css">
<style type="text/css">
    .disclaimer {
        margin: 20px;
        font-size: 20px;
        color: red;
    }

    .selectcategory {
        border: solid 1px #00000021;
        box-shadow: 0 4 4 0 rgba(0, 0, 0, 0.6);
        text-align: center;
    }

    .wrappermy {
        position: relative;
        width: 25px;
        height: 85px;
        margin-left: 45%;
    }

    .alert-danger,
    .alert-success {
        padding: 7px;
    }

    @media only screen and (min-width:769px) {
        .default-icon {
            float: right;
            margin-top: -65px;
            margin-left: 0;
        }
    }

    @media only screen and (max-width:768px) {
        #icon-holder {
            float: left;
            margin-left: 10px;
        }
    }

    @media only screen and (max-width:768px) {
        .input-boxes {
            margin-left: 0;
            margin-right: 0;
        }

        .link-btn {
            margin-right: 17px;
        }
    }

    @media only screen and (max-width:375px) {
        .Zebra_DatePicker_Icon_Wrapper {
            margin-top: 0;
            margin-left: -3px;
            width: 49% !important;
        }

        #datepicker-example111 {
            top: -5px !important;
        }

        .Zebra_DatePicker_Icon {
            margin: 3px 8px 0 0 !important;
        }

        .input-to {
            margin-top: -50px !important;
            margin-left: 9px !important;
            width: 50% !important;
        }

        #datepicker-example1 {
            width: 95% !important;
            float: left;
        }

        .to-margin {
            margin-top: -50px !important;
        }

        button .Zebra_DatePicker_Icon_Inside_Right:nth-child(2) {
            margin-top: -15px !important;
        }

        .remove-button {
            right: -30px;
            top: -31px;
        }

        .edit-button {
            right: -33px;
            top: -68px;
        }

        .Zebra_DatePicker_Icon_Wrapper {
            margin-top: 0;
            margin-left: -3px;
            width: 49% !important;
        }

        #datepicker-example111 {
            top: -5px !important;
        }

        .Zebra_DatePicker_Icon {
            margin: 3px 8px 0 0 !important;
        }

        .input-to {
            margin-top: -50px !important;
            margin-left: 9px !important;
            width: 50% !important;
        }

        #datepicker-example1 {
            width: 95% !important;
            float: left;
        }

        .image-holder-profile {
            float: right;
        }

        .image-holder {
            float: left;
            padding-left: 23px;
        }

        .second-box-model .form-group input {
            width: 100% !important;

        }

        .Zebra_DatePicker_Icon .Zebra_DatePicker_Icon_Inside_Right {
            margin-top: -80px !important;
        }
    }

    @media only screen and (max-width:320px) {
        .edit-button {
            right: -26px;
            top: -68px;
        }

        .remove-button {
            right: -26px;
            top: -31px;
        }

        .available {
            font-weight: normal;
            font-size: 13px;
        }

        .navbar-brand {
            width: 73%;
            margin: 7px;
        }
    }

    .ui-autocomplete {
        z-index: 99999999 !important;
    }

    .ui-autocomplete.ui-front {
        z-index: 1051;
    }
</style>
<body>
    <div class="inner-content-box test-complete-box" style="padding-top: 50px;padding-bottom: 50px;">
        <div class="container">
            <div class="client-account-box">
                <div class="form-box clearfix top">
        <div class="top col-md-12">
                                <div class="row input-boxes" style="padding-top: 50px; padding-bottom: 50px;">
                                    <div class="col-md-12">
                                        <center>
                                            <div style="font-size: 20px;font-weight: bold;padding-top: 30px;padding-bottom: 10px">Thank you for your interest. We have received your request and someone from econolytics would contact you in the next 2 working days.
                                            </div>
                                        </center>
                                    </div>
                                    <br><br>
                                    <center><a href='freelance-profile.php' class='btn btn-primary'>OK</a></center>
                                </div>
                            </div>
                            <!-- .row .input-boxes -->

                    </div>
                    <!-- .form-left-box -->
            </div>
        </div>
    </div>
    <?php include "footer.php"; ?>
    <?php //include "include/signup.php"; ?>

</body>
</html>