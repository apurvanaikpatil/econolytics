<?php

session_start();
if (($_SESSION['freelance_email'] == '')) {
    header('Location:index.php?signin=1');
    exit;
}

include_once "include/config.inc.php";
include_once 'ecomail/mailer/send_mail.php';

$mode = $_GET["mode"];

if($mode == 'i')
{
	$agency_id = $_SESSION['agency_id'];
	$name = $_POST["analyst_name"];
	$email_id = $_POST["analyst_email"];
	$phone = $_POST["analyst_phone_number"];
	$domain = $_POST["analyst_domain_expertise"];
	$analyst_password = $_POST["analyst_password"];

	//--apurva-code-start-(25 may 2021)--//
	
	if($email_id == "")
	{
		$is_set_email = "no";

		$name_str = substr($name, 0,3);
		$email_id = $name_str.'_'.$agency_id;
	}
	else
	{
		$is_set_email = "yes";
	}
	
	$agency_sql = "SELECT * FROM agency_details WHERE id = '$agency_id'";
    $agency_data = mysqli_query($dbh,$agency_sql);
    $agency_arr = resultantArray($agency_data);
    $agency_name = $agency_arr[0]['name'];
    $agency_email = $agency_arr[0]['email'];   
	//--apurva-code-end-(25 may 2021)--//

	$analyst_details = mysqli_query($dbh, "SELECT id AS analyst_id, email_id AS analyst_email_id, name AS analyst_name FROM employee WHERE email_id = '$email_id' and email_id !=''");
	
		
	if(mysqli_num_rows($analyst_details) > 0)
	{
		$analyst = mysqli_fetch_array($analyst_details);
		extract($analyst);

		$flag_query = "update"; 
		
		$query = "UPDATE employee SET 
			agency_id = $agency_id,
			name = '$name',
			phone = $phone,
			domain = '$domain'
			WHERE id = $analyst_id";
	}
	else
	{ 
		$flag_query = "insert";
		
		$query = "INSERT INTO employee(
			agency_id,
			name,
			email_id,
			password,
			phone,
			domain,
			active_deactive
			) VALUES(
			'$agency_id',
			'$name',
			'$email_id',
			'".md5($analyst_password)."',
			'$phone',
			'$domain',
			'active'
			)";
	}

	if(mysqli_query($dbh,$query))
	{	
		//echo "<pre>"; print_r($flag_query); echo "</pre>";
		//echo "<pre>"; print_r($is_set_email); echo "</pre>";
		
		if($flag_query == "insert" && $is_set_email == 'no')
		{
			$employee_id = mysqli_insert_id($dbh);

			//----new_email----//
			$name_str = substr($name, 0,3);
			$email_id = $employee_id.'_'.$agency_id.'_'.$name_str.'@gmail.com';
			
			//----update----//
			$update_query = "UPDATE employee SET 
			email_id = '$email_id'
			WHERE id = $employee_id";

			mysqli_query($dbh,$update_query);
		}
		
		$agency_details = mysqli_query($dbh, "SELECT a.name AS agency_name, a.id AS aid, e.name AS agency_analyst_name FROM agency_details a INNER JOIN employee e ON a.analyst_id = e.id WHERE a.id = $agency_id LIMIT 1");

		extract(mysqli_fetch_array($agency_details));
		
		//mail code
		if(mysqli_num_rows($analyst_details) > 0)
		{
			$params = array(
	            'analyst_name' => $analyst_name,
	            'agency_analyst_name' => $agency_analyst_name,
	            'agency_name' => $agency_name,
	            'analyst_id' => $analyst_id,
	            'agency_id' => $aid
	        );

	        $to = $analyst_email_id;
        	$to_name = $analyst_name;
		}
		else
		{
			$params = array(
	            'analyst_name' => $name,
	            'agency_analyst_name' => $agency_analyst_name,
	            'agency_name' => $agency_name,
	            'analyst_id' => $analyst_id,
	            'agency_id' => $aid
	        );

	        $to = $email_id;
        	$to_name = $name;
		}		
		
        //same mail to econolytics
        /*$user_indicator = 3;
        $attachment = "";
        $mailStatus = create_n_send(106, $params, $to, $to_name, $dbh, $attachment, $user_indicator);*/

        //same mail to AgencyOwner
        $params_analyst = array(
	            'analyst_name' => $name,
	            'analyst_email' => $email_id,
	            'analyst_password' => $analyst_password,
	            'agency_analyst_name' => $agency_analyst_name,
	            'agency_name' => $agency_name,
	            'analyst_id' => $analyst_id,
	            'agency_id' => $aid
	        );
        
        $user_indicator = 4;
        $attachment = "";
        $mailStatus = create_n_send(106, $params_analyst, $agency_email, $agency_name, $dbh, $attachment, $user_indicator);		

		header('Location: analyst.php');
	}
	else
	{
		die(mysqli_error(($dbh)));
	}
}

if($mode == 'd')
{
	$employee_id = $_GET["id"];
	
	$query = "UPDATE employee SET agency_id = NULL WHERE id = '$employee_id'";

	if(mysqli_query($dbh,$query))
	{
		header('Location: analyst.php');
	}
	else
	{
		die(mysqli_error(($dbh)));
	}
}

if($mode == 'ad') //analyst delete his account
{
	$employee_id = $_GET["id"];
	
	$query = "UPDATE employee SET agency_id = NULL WHERE id = '$employee_id'";

	if(mysqli_query($dbh,$query))
	{
		header('Location: freelance-profile.php');
	}
	else
	{
		die(mysqli_error(($dbh) . "Query :- " . $query));
	}
}

if($mode == 'e')
{
	$employee_id = $_GET["id"];

	$query = "SELECT email_id, name FROM employee WHERE id = '$employee_id'";

	$data = mysqli_query($dbh, $query);
	$employee = mysqli_fetch_array($data);
	extract($employee);

	$_SESSION["freelance_email"] = $email_id;
	$_SESSION['freelance_fname'] = $name;
    $_SESSION['freelance'] = 'freelancer';
    $_SESSION['is_agency_login'] = 1;

	header('Location: freelance-profile.php');
}

//on clicking of back button in view analyst, switch the login to the agency owner login [aol => 'agency owner login']
if($mode == 'aol')
{
	$query = "SELECT agency_id FROM employee WHERE email_id = '" . $_SESSION["freelance_email"] . "'";
	$data = mysqli_query($dbh, $query);
	$agency_details = mysqli_fetch_assoc($data);
	extract($agency_details);

	$query1 = "SELECT e.email_id, e.name FROM employee e INNER JOIN agency_details a ON a.analyst_id = e.id WHERE a.id = $agency_id";
	$data1 = mysqli_query($dbh, $query1);
	$agency_owner_details = mysqli_fetch_assoc($data1);
	extract($agency_owner_details);

	$_SESSION["freelance_email"] = $email_id;
	$_SESSION['freelance_fname'] = $name;
    $_SESSION['freelance'] = 'freelancer';
    $_SESSION['is_agency_login'] = 0;
    header('Location: analyst.php');
}
?>